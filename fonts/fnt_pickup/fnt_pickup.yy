{
    "id": "fd04072b-ab20-4492-9e11-1e899bc62881",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_pickup",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "SimSun",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e050aeb3-99b8-4eb0-a18d-78f3261fa24b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8d140669-989a-4150-816a-c29f8ad611c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 38,
                "offset": 6,
                "shift": 17,
                "w": 4,
                "x": 187,
                "y": 202
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e7ab18fc-1991-44d9-b165-46d6b1516064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 11,
                "x": 57,
                "y": 202
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7175aed6-a280-4bc4-9017-1131b5480b2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 82
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ae59f36a-3d54-4d4a-a93d-8f23545199b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 2,
                "y": 202
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "dac99e87-cc80-4d1a-81c8-9f2e974c6f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "78e9e2b9-d446-45b7-84e8-8abef177b28a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3d1cec46-1fca-4767-ad36-d8fb6a38c4ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 5,
                "x": 180,
                "y": 202
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "976e67e8-6de8-4964-a2d6-0b65870d8883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 7,
                "shift": 17,
                "w": 8,
                "x": 126,
                "y": 202
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "336c3868-2b76-4c26-b4d6-0404c4d399ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 8,
                "x": 136,
                "y": 202
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "742a2c0d-ca9b-4259-aa10-1d5ef3f46b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 82
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "590331b9-f2ae-4b81-b03b-67826567a441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 48,
                "y": 162
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "32c20d05-0cbc-4c79-ba2e-d309a5b837ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 5,
                "x": 173,
                "y": 202
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5fedd10b-3d85-4eca-a61b-284bd6753ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 164,
                "y": 122
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7fab1433-207d-4bf1-bd02-c128cd1cb259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 4,
                "x": 193,
                "y": 202
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5d70ebaa-9031-4003-af3f-9cad76e44b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "64a106ec-8409-4c7f-b48b-2268d7f6425d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 183,
                "y": 162
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0a1d7b92-9615-40c7-bf56-6353cafb9843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 38,
                "offset": 3,
                "shift": 17,
                "w": 10,
                "x": 82,
                "y": 202
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "87299b6c-6dbc-4bca-bea5-d1510fa5ca2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 138,
                "y": 162
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8f4b09e1-c291-4902-b722-0ec5ceb59c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 153,
                "y": 162
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b2101481-6099-42fe-9740-9595eadd1dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 122
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7c83235b-d84e-4edd-9c1a-42bfa1851cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 198,
                "y": 162
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a7c127bf-3663-4940-a27a-1c2471bca633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 18,
                "y": 162
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "141bc356-8035-4baf-9255-cc7c438a5ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 33,
                "y": 162
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "730a138d-3e11-4106-8218-b7e7f7fca283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 212,
                "y": 122
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "eb2f9ad3-f53e-43ed-9979-758fb57518b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 196,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0a09bfe4-e432-45c7-8846-4ca32acb2fde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 38,
                "offset": 6,
                "shift": 17,
                "w": 4,
                "x": 199,
                "y": 202
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "baf7a560-3820-459b-892e-012c1f81f43e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 38,
                "offset": 7,
                "shift": 17,
                "w": 3,
                "x": 205,
                "y": 202
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5e546ac2-4464-41a4-8839-f082830473e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 213,
                "y": 162
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4d45ef95-b9ae-4c72-9dbe-66051e224dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2af379fa-826a-49c2-87cd-46316e521d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 16,
                "y": 202
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4ee722ce-1441-4c28-ab62-06c5b7fdec69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 180,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "dcf6e9b8-3bf5-41de-ac5a-15e1ae0a78ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 82
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "deb52ccc-5cf8-40a5-adf2-4fe2a96647b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "535174b5-1da0-46e1-9679-41b48344bb20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 155,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "08eabd88-0b26-44bf-b951-684cc0a18a24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 223,
                "y": 82
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "eaaa0ab2-b875-43c1-8740-e055ac9140fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9935758c-21e5-419d-aed4-b0cccb4caa87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 143,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4e04e40b-2b98-4ed5-b260-3ae608d64eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 56,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0631f41b-b72e-4a11-b505-df30b5f3684c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 38,
                "y": 42
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d5b382b7-5864-4db1-8606-88b24fc76ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2b564937-c821-4812-bdce-e137848e1389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 30,
                "y": 202
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ff0c4899-0322-47a2-8ff1-2b697bc784a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 20,
                "y": 42
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d4d09246-a135-4785-98a4-78bb9482a04d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 160,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "af9e5963-f5cd-4c7a-8d1f-a1a08f68e596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "74fbe3d3-c9ab-4a30-bae1-852ddee0e53b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "95a1601a-1c37-4b51-a5a7-78824b071b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 38,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "85e67432-d539-4c99-b74a-ff1bfe64ce2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 82
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4ceec39f-bfce-4b9c-a0c3-aa385190f00a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 74,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2d1b660f-ce9e-4511-a67b-e0066d2ee386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 82
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "338b96fb-d900-43fd-95d1-6ff65f2e83ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 177,
                "y": 42
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f72dabde-6799-40fb-9c1d-0638ff4469bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 228,
                "y": 122
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "80fbd435-0fd0-43ad-87c4-bc21d90d018b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 148,
                "y": 122
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7c37995c-1317-4b81-bc49-0b165eae25cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e67c695e-e0ff-4d77-8603-8a360621cab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 194,
                "y": 42
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d04cd733-163b-41ea-809e-b97cd8f352fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ea0c625a-1d5a-4ab1-90cc-ea6fafcd20a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8d0b15be-787d-44b7-a57c-41a694c7c1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 132,
                "y": 122
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "53bb1a12-69ec-4e27-bcf3-6c548aca024f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 228,
                "y": 42
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6c08f5af-52a1-4142-94fe-4b4a4dddf605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 6,
                "shift": 17,
                "w": 9,
                "x": 94,
                "y": 202
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "59a719d3-cb2d-432b-bc0e-03f3875c7df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 63,
                "y": 162
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b27cc1e4-b808-41f7-8820-717d94131c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 8,
                "x": 116,
                "y": 202
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9bebeaff-e848-49c2-8647-de374803950f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 38,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 105,
                "y": 202
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "848314a4-9add-497e-a14a-fa63c1ba104c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ec9c53bc-b710-45bf-8c07-027c0d91e666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 3,
                "shift": 17,
                "w": 7,
                "x": 164,
                "y": 202
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "332c7daf-1812-4aa9-bf4c-42d52e81f514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d09a1ced-8df2-41f1-b094-5984e9ad02e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 116,
                "y": 122
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e1c2c59d-6eed-4f4b-8242-61d9f7ed0aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 168,
                "y": 162
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b3ea3d4c-02f9-45a2-b106-e6d71ab2bce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 100,
                "y": 122
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6e79026c-cf7f-4451-8ad4-8f7adbba53b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 78,
                "y": 162
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "cb1a9837-c668-4207-8b79-a67553dab665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 84,
                "y": 122
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2467f937-df4d-4308-929a-ed035f89dee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 68,
                "y": 122
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a4bf04bd-55eb-42bc-9248-a857accf8fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2b983778-97f3-48f0-ba93-4dc948c07665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 38,
                "offset": 3,
                "shift": 17,
                "w": 10,
                "x": 70,
                "y": 202
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "293765f0-2faf-4afc-8635-914727e57e8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 44,
                "y": 202
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ebcf500b-62eb-4534-92c6-b70dd764b8cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 82
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0cf6aa87-ba69-4db7-a2a0-cba111f7012c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 241,
                "y": 162
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "46930249-5a87-4f0b-bb7b-b2a719d1e40c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 38,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bc20562c-6aa5-4516-bc90-dbe67e507d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 82
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "34904902-b458-4f73-beb8-1b6a88b45751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 123,
                "y": 162
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "90316a85-8a48-424b-88e3-70440e535b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 52,
                "y": 122
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a8452b15-d1ee-46f7-827e-f8386643613f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 36,
                "y": 122
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4c82756d-053b-4806-8027-ca792ec33ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 82
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9925077a-147f-4ce8-8ac4-d40ac22beac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 38,
                "offset": 3,
                "shift": 17,
                "w": 12,
                "x": 227,
                "y": 162
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "debc5f86-d489-40ef-a895-2afc08dd732a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 108,
                "y": 162
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b7965015-a0c8-4cac-b26e-f3f8dfae71de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 82
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ae07b0f0-b747-43cb-9b6a-12b48dc179be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 82
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6889b94b-88c6-4da8-afbe-eb16f80f4fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1cc4be68-53b8-459d-972f-9f61a1d412d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 109,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ba26448d-7aea-415c-99a3-19d9f2324b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 126,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8121e20a-2815-4384-9475-267c0a55e136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 93,
                "y": 162
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "df201de5-a4a6-4bf0-8dbc-2f8556fe78dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 7,
                "shift": 17,
                "w": 7,
                "x": 155,
                "y": 202
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e9c6ecce-2560-44dd-b7c8-e94818b06078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 8,
                "shift": 17,
                "w": 1,
                "x": 210,
                "y": 202
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f20211d0-5ede-4eb9-a9a3-d6121547593f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 3,
                "shift": 17,
                "w": 7,
                "x": 146,
                "y": 202
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e8cadab5-87f1-431d-9b63-89f7b85f818f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 92,
                "y": 42
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "e13894ce-a0b4-4db9-ad90-0496af4945c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 0,
                "x": 213,
                "y": 202
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 25,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}