{
    "id": "ab404a78-fa33-46f6-bd9e-c136d201795c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 4,
    "bbox_right": 253,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bf7580e-7bb7-4f9d-99a6-6e0f26130d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab404a78-fa33-46f6-bd9e-c136d201795c",
            "compositeImage": {
                "id": "8ce6450e-a480-46b8-bef0-61dae3271259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bf7580e-7bb7-4f9d-99a6-6e0f26130d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94490406-06ff-48fc-a33c-4c92a81d3556",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bf7580e-7bb7-4f9d-99a6-6e0f26130d3a",
                    "LayerId": "8cb3ce78-36e0-4dbc-ab19-0f4d84be0fda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 259,
    "layers": [
        {
            "id": "8cb3ce78-36e0-4dbc-ab19-0f4d84be0fda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab404a78-fa33-46f6-bd9e-c136d201795c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 257,
    "xorig": 128,
    "yorig": 129
}