{
    "id": "db242b74-b561-42d9-b144-c8603a8ce654",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_bush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 4,
    "bbox_right": 51,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "363304f0-e11a-4ca0-a2ee-0d66be04d746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db242b74-b561-42d9-b144-c8603a8ce654",
            "compositeImage": {
                "id": "738e7f86-2cdb-4672-953d-79ac0f25cdb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "363304f0-e11a-4ca0-a2ee-0d66be04d746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18fe8be6-d95d-4418-b539-1f8883d68c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "363304f0-e11a-4ca0-a2ee-0d66be04d746",
                    "LayerId": "69b9bf45-f68d-4862-b272-039ac7120939"
                }
            ]
        },
        {
            "id": "95408846-2fa5-4a95-9535-b30a50d90cc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db242b74-b561-42d9-b144-c8603a8ce654",
            "compositeImage": {
                "id": "504e1464-5204-4948-8391-ee56de3bebb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95408846-2fa5-4a95-9535-b30a50d90cc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8ca18e4-8e33-4fcd-9a08-318b5ead85b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95408846-2fa5-4a95-9535-b30a50d90cc9",
                    "LayerId": "69b9bf45-f68d-4862-b272-039ac7120939"
                }
            ]
        },
        {
            "id": "b22fd183-4a14-4c72-aaed-aec5d7668716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db242b74-b561-42d9-b144-c8603a8ce654",
            "compositeImage": {
                "id": "e99a64ab-5af6-414e-8517-307e65804574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22fd183-4a14-4c72-aaed-aec5d7668716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ed9cc3b-1750-4bb9-b06e-616ce44ca40b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22fd183-4a14-4c72-aaed-aec5d7668716",
                    "LayerId": "69b9bf45-f68d-4862-b272-039ac7120939"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "69b9bf45-f68d-4862-b272-039ac7120939",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db242b74-b561-42d9-b144-c8603a8ce654",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 28,
    "yorig": 39
}