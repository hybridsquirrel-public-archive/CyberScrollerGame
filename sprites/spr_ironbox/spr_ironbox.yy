{
    "id": "411c74dc-cc15-43bc-9795-f9c363dcc3c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ironbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "beee6d6f-7db3-49ee-94eb-055dce0b1c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "411c74dc-cc15-43bc-9795-f9c363dcc3c9",
            "compositeImage": {
                "id": "c4fbe116-8b72-4f77-ac80-664ee58b4ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beee6d6f-7db3-49ee-94eb-055dce0b1c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cf3a09b-9754-4341-a55c-ed02ddb7cbbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beee6d6f-7db3-49ee-94eb-055dce0b1c9e",
                    "LayerId": "addb96c3-e40e-4145-8930-feb4fd95353b"
                }
            ]
        },
        {
            "id": "b9130062-9796-43a3-b69d-bdc1d88ab574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "411c74dc-cc15-43bc-9795-f9c363dcc3c9",
            "compositeImage": {
                "id": "ef194386-d3ba-4bcc-81ee-bff92d16358d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9130062-9796-43a3-b69d-bdc1d88ab574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474b208d-ec2e-40ee-ad87-9905fa122d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9130062-9796-43a3-b69d-bdc1d88ab574",
                    "LayerId": "addb96c3-e40e-4145-8930-feb4fd95353b"
                }
            ]
        },
        {
            "id": "96b127f6-f0f7-4cc8-81af-78bd5f7ae8e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "411c74dc-cc15-43bc-9795-f9c363dcc3c9",
            "compositeImage": {
                "id": "bbd0ef92-17bc-4385-a46b-9b91e7070b28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b127f6-f0f7-4cc8-81af-78bd5f7ae8e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12b1d12c-e42d-4e8a-acaf-4ff64398d7bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b127f6-f0f7-4cc8-81af-78bd5f7ae8e2",
                    "LayerId": "addb96c3-e40e-4145-8930-feb4fd95353b"
                }
            ]
        },
        {
            "id": "a5a99a3a-ea92-4cf3-8d68-6d327644f458",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "411c74dc-cc15-43bc-9795-f9c363dcc3c9",
            "compositeImage": {
                "id": "5eef0d09-2e0b-4275-aacf-0e127ac4a918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5a99a3a-ea92-4cf3-8d68-6d327644f458",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ae3324b-d162-4677-97be-b3e6905c59bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5a99a3a-ea92-4cf3-8d68-6d327644f458",
                    "LayerId": "addb96c3-e40e-4145-8930-feb4fd95353b"
                }
            ]
        },
        {
            "id": "55f77538-e56f-45f5-bc48-c21a2091c72a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "411c74dc-cc15-43bc-9795-f9c363dcc3c9",
            "compositeImage": {
                "id": "86cefec7-00ea-4e5e-8e90-0215b2c97425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55f77538-e56f-45f5-bc48-c21a2091c72a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d13a6330-46fc-4b86-bfaf-48a785ab713b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55f77538-e56f-45f5-bc48-c21a2091c72a",
                    "LayerId": "addb96c3-e40e-4145-8930-feb4fd95353b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "addb96c3-e40e-4145-8930-feb4fd95353b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "411c74dc-cc15-43bc-9795-f9c363dcc3c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}