{
    "id": "93582c2e-dbc2-4545-b27d-2dae1edcbcbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_oilbarrelcluster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 10,
    "bbox_right": 28,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bd29eb1-33e5-47c6-84f2-1f69a4411fb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93582c2e-dbc2-4545-b27d-2dae1edcbcbb",
            "compositeImage": {
                "id": "f00330aa-1855-4049-8ca8-fe7cce992a6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bd29eb1-33e5-47c6-84f2-1f69a4411fb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8603140e-ec88-443e-b701-98578436855a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bd29eb1-33e5-47c6-84f2-1f69a4411fb2",
                    "LayerId": "a588f4e4-48b4-4469-b2b9-5e50f6573136"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "a588f4e4-48b4-4469-b2b9-5e50f6573136",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93582c2e-dbc2-4545-b27d-2dae1edcbcbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 19,
    "yorig": 20
}