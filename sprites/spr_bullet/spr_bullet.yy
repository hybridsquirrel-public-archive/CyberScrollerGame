{
    "id": "617e8c75-596e-4f9c-806b-6c929af3eaa2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39ef6998-7cf7-4f74-ae61-0616b9bc9d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "617e8c75-596e-4f9c-806b-6c929af3eaa2",
            "compositeImage": {
                "id": "00034bcf-29af-447d-a03c-4af61965b4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39ef6998-7cf7-4f74-ae61-0616b9bc9d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268da627-b092-4130-b087-18b0fd4bd150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ef6998-7cf7-4f74-ae61-0616b9bc9d4c",
                    "LayerId": "bceaefa6-1b32-4ef3-acbb-cb0693681ddb"
                }
            ]
        },
        {
            "id": "4007f499-2cf7-4790-9fef-8072882e19c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "617e8c75-596e-4f9c-806b-6c929af3eaa2",
            "compositeImage": {
                "id": "d0b9f922-4f7c-43b6-bc1b-3406fa514ef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4007f499-2cf7-4790-9fef-8072882e19c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e6e0359-8f93-4481-b60d-ec22d9d28aa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4007f499-2cf7-4790-9fef-8072882e19c9",
                    "LayerId": "bceaefa6-1b32-4ef3-acbb-cb0693681ddb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "bceaefa6-1b32-4ef3-acbb-cb0693681ddb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "617e8c75-596e-4f9c-806b-6c929af3eaa2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 9
}