{
    "id": "cf3485e5-5029-49b4-84e2-d703569b3b97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gargoyle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 0,
    "bbox_right": 56,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b08a2db5-844c-4914-9abb-0c4eeabb48bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf3485e5-5029-49b4-84e2-d703569b3b97",
            "compositeImage": {
                "id": "3aba9b8c-a6ca-467d-a03f-ea268dfdee93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b08a2db5-844c-4914-9abb-0c4eeabb48bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f280da6-40b6-42e2-824a-8f5e79c78fff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08a2db5-844c-4914-9abb-0c4eeabb48bf",
                    "LayerId": "d3e244e4-fef4-45e6-a017-7fa2fca062f1"
                }
            ]
        },
        {
            "id": "5aa676bf-b4af-4457-9521-5b60005b536a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf3485e5-5029-49b4-84e2-d703569b3b97",
            "compositeImage": {
                "id": "419504ee-d213-4be6-a174-95796bb10665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aa676bf-b4af-4457-9521-5b60005b536a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7f90ab2-8cde-4d5a-b199-e43bede67f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aa676bf-b4af-4457-9521-5b60005b536a",
                    "LayerId": "d3e244e4-fef4-45e6-a017-7fa2fca062f1"
                }
            ]
        },
        {
            "id": "d5e7e388-3e95-45df-a691-43a9489e38d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf3485e5-5029-49b4-84e2-d703569b3b97",
            "compositeImage": {
                "id": "805dd7b5-9195-43aa-b926-5e72ef9e2daa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e7e388-3e95-45df-a691-43a9489e38d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58c20aa1-73a1-46a1-9581-4351d27a2bdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e7e388-3e95-45df-a691-43a9489e38d8",
                    "LayerId": "d3e244e4-fef4-45e6-a017-7fa2fca062f1"
                }
            ]
        },
        {
            "id": "ef952021-e4ff-41e5-b042-d6347f4e83a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf3485e5-5029-49b4-84e2-d703569b3b97",
            "compositeImage": {
                "id": "30783e55-c6af-4848-a9ff-596109e93476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef952021-e4ff-41e5-b042-d6347f4e83a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e56b19a-2142-4833-818d-63a7c8142e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef952021-e4ff-41e5-b042-d6347f4e83a9",
                    "LayerId": "d3e244e4-fef4-45e6-a017-7fa2fca062f1"
                }
            ]
        },
        {
            "id": "ae8d2576-ca25-4ed9-803b-26608b85b7ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf3485e5-5029-49b4-84e2-d703569b3b97",
            "compositeImage": {
                "id": "bf3e7613-ea67-40a2-8f8c-46dc7bdbc8e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8d2576-ca25-4ed9-803b-26608b85b7ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01dfc22c-c394-46aa-9bca-42e501076335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8d2576-ca25-4ed9-803b-26608b85b7ac",
                    "LayerId": "d3e244e4-fef4-45e6-a017-7fa2fca062f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d3e244e4-fef4-45e6-a017-7fa2fca062f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf3485e5-5029-49b4-84e2-d703569b3b97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 37,
    "yorig": 29
}