{
    "id": "e0a8dcc4-def9-44ec-abbb-82f434a13be6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemydeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ab4d4e5-c8a1-4bd5-bd14-909f2154d129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a8dcc4-def9-44ec-abbb-82f434a13be6",
            "compositeImage": {
                "id": "b15abce8-5b5c-4f33-be68-1fecaa198be0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab4d4e5-c8a1-4bd5-bd14-909f2154d129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de64e02-7939-4fdc-8648-d06a4ed20aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab4d4e5-c8a1-4bd5-bd14-909f2154d129",
                    "LayerId": "ac803006-c663-4b42-8734-477e11fc3b17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ac803006-c663-4b42-8734-477e11fc3b17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0a8dcc4-def9-44ec-abbb-82f434a13be6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 51
}