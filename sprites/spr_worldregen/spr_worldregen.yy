{
    "id": "1defeedb-f06e-447c-b3ca-52cd0448a843",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_worldregen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ac665a0-3fb8-4f23-9434-ee99ea744d6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1defeedb-f06e-447c-b3ca-52cd0448a843",
            "compositeImage": {
                "id": "65bd6966-f3ba-4ec2-9bda-57724d864c53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ac665a0-3fb8-4f23-9434-ee99ea744d6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b08692d9-4da4-438d-b8cf-b7ba2594b5e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ac665a0-3fb8-4f23-9434-ee99ea744d6b",
                    "LayerId": "d3e500f7-aa07-4550-9ce0-ee1b47ee39e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d3e500f7-aa07-4550-9ce0-ee1b47ee39e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1defeedb-f06e-447c-b3ca-52cd0448a843",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}