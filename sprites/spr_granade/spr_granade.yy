{
    "id": "1488c194-9014-432c-bdc3-918d933790aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_granade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 29,
    "bbox_right": 35,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a68405e-7313-4640-8f82-3e277bfe7c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1488c194-9014-432c-bdc3-918d933790aa",
            "compositeImage": {
                "id": "5be90d16-c505-4bc1-9977-d4c471dd3fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a68405e-7313-4640-8f82-3e277bfe7c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9021418-629e-4fd9-92d3-324816b30f32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a68405e-7313-4640-8f82-3e277bfe7c0f",
                    "LayerId": "9d0bde55-ee59-4694-92b1-98306f315fb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9d0bde55-ee59-4694-92b1-98306f315fb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1488c194-9014-432c-bdc3-918d933790aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}