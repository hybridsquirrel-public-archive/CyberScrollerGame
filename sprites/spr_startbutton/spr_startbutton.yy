{
    "id": "8386847a-676a-48bc-8e32-8d2d78d17dd6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 268,
    "bbox_left": 22,
    "bbox_right": 277,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe0c3601-43fc-43ac-add8-06e7aab0df1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8386847a-676a-48bc-8e32-8d2d78d17dd6",
            "compositeImage": {
                "id": "28cb46f6-6b45-4a14-9249-810c0c750d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe0c3601-43fc-43ac-add8-06e7aab0df1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34623136-9eaa-4665-b225-a888ea85ca25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe0c3601-43fc-43ac-add8-06e7aab0df1b",
                    "LayerId": "6880348c-1932-4672-b5f3-9557768c3a6a"
                }
            ]
        },
        {
            "id": "74a59306-207b-42eb-8fa3-1223891a0d54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8386847a-676a-48bc-8e32-8d2d78d17dd6",
            "compositeImage": {
                "id": "1f1384d4-ae82-47bc-8d7b-220402b337b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74a59306-207b-42eb-8fa3-1223891a0d54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66be2131-4a17-4f2c-be1f-1fbed97c8f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74a59306-207b-42eb-8fa3-1223891a0d54",
                    "LayerId": "6880348c-1932-4672-b5f3-9557768c3a6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 269,
    "layers": [
        {
            "id": "6880348c-1932-4672-b5f3-9557768c3a6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8386847a-676a-48bc-8e32-8d2d78d17dd6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 289,
    "xorig": 144,
    "yorig": 134
}