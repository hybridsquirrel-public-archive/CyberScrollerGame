{
    "id": "5bc83ca7-0f3c-4c26-b085-85ea68f975da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startmenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbe80ae3-cc38-4757-b65d-9a2f70912ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc83ca7-0f3c-4c26-b085-85ea68f975da",
            "compositeImage": {
                "id": "31e0f166-c986-46a7-a08e-7afed81899e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe80ae3-cc38-4757-b65d-9a2f70912ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08160bea-17bd-48fe-870e-311def579903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe80ae3-cc38-4757-b65d-9a2f70912ad9",
                    "LayerId": "afd55b4e-8da6-40bc-a04f-cd088aa45cc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "afd55b4e-8da6-40bc-a04f-cd088aa45cc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bc83ca7-0f3c-4c26-b085-85ea68f975da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}