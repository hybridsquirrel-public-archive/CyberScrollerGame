{
    "id": "26ea1ebb-64f7-4de0-b7b0-e17ab1fd0e56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_curser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 4,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6a4c10a-df5a-4133-9521-b6b26d91eef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ea1ebb-64f7-4de0-b7b0-e17ab1fd0e56",
            "compositeImage": {
                "id": "eba0e0a9-d71c-4225-ad4e-7ca3753b2847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6a4c10a-df5a-4133-9521-b6b26d91eef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05cd0ca4-e2fe-42f1-8adb-56f52844014b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6a4c10a-df5a-4133-9521-b6b26d91eef8",
                    "LayerId": "e1d2dd80-1dd7-4698-b7b9-b551d3df0ac1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "e1d2dd80-1dd7-4698-b7b9-b551d3df0ac1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26ea1ebb-64f7-4de0-b7b0-e17ab1fd0e56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}