{
    "id": "687c8192-2ba1-4f96-ae75-a04f3c868b25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 267,
    "bbox_left": 5,
    "bbox_right": 262,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a54fe03-a953-4837-b4ca-d4a3a4d2e59c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "687c8192-2ba1-4f96-ae75-a04f3c868b25",
            "compositeImage": {
                "id": "9c81076c-e7cb-4872-9826-ae18ed212291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a54fe03-a953-4837-b4ca-d4a3a4d2e59c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "917d6a64-98ae-4b04-9026-faf27fa072fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a54fe03-a953-4837-b4ca-d4a3a4d2e59c",
                    "LayerId": "2d24cf74-8c3f-4aba-9f69-66f47e5d3833"
                }
            ]
        },
        {
            "id": "4507c3ca-757c-4063-8db4-0381952c0bdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "687c8192-2ba1-4f96-ae75-a04f3c868b25",
            "compositeImage": {
                "id": "91b117ec-2730-4f04-8186-ac57a160e6e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4507c3ca-757c-4063-8db4-0381952c0bdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a97838e-2e4c-40b6-8ad5-50894ce5a21b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4507c3ca-757c-4063-8db4-0381952c0bdd",
                    "LayerId": "2d24cf74-8c3f-4aba-9f69-66f47e5d3833"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "2d24cf74-8c3f-4aba-9f69-66f47e5d3833",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "687c8192-2ba1-4f96-ae75-a04f3c868b25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 266,
    "xorig": 133,
    "yorig": 135
}