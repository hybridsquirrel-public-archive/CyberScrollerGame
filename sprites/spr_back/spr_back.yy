{
    "id": "300ea9e8-5db3-4b1f-91cf-6ea71ef0f601",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 265,
    "bbox_left": 20,
    "bbox_right": 275,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bb0583c-f435-4575-9441-494f9023f01a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "300ea9e8-5db3-4b1f-91cf-6ea71ef0f601",
            "compositeImage": {
                "id": "9c58beeb-c4b0-4bfd-a9b0-68e55d64d95e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb0583c-f435-4575-9441-494f9023f01a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c298c5da-515e-4851-b9b8-448e273923e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb0583c-f435-4575-9441-494f9023f01a",
                    "LayerId": "1ff8a27f-527f-4f98-b785-e67298c00a9d"
                }
            ]
        },
        {
            "id": "02a04e06-21a4-4245-b904-40061c9f36b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "300ea9e8-5db3-4b1f-91cf-6ea71ef0f601",
            "compositeImage": {
                "id": "17b1844f-c6b6-47da-a764-ca2956972edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02a04e06-21a4-4245-b904-40061c9f36b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5f1a74-e098-4ee9-9545-ca5b99f533b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02a04e06-21a4-4245-b904-40061c9f36b1",
                    "LayerId": "1ff8a27f-527f-4f98-b785-e67298c00a9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "1ff8a27f-527f-4f98-b785-e67298c00a9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "300ea9e8-5db3-4b1f-91cf-6ea71ef0f601",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 294,
    "xorig": 147,
    "yorig": 135
}