{
    "id": "9c84d41a-552a-4883-bdba-772a4e7ba5d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_howtop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0c9b1cb-f54f-42c7-af38-73c87fadcce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c84d41a-552a-4883-bdba-772a4e7ba5d9",
            "compositeImage": {
                "id": "b44ae195-93eb-46be-85c4-f4f0e8ddff5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c9b1cb-f54f-42c7-af38-73c87fadcce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76a1fe7e-5938-4dfe-bd6a-24d6b0dc6045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c9b1cb-f54f-42c7-af38-73c87fadcce2",
                    "LayerId": "cde2982a-7ce9-4e36-b2e5-ddcdf9f3bf64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "cde2982a-7ce9-4e36-b2e5-ddcdf9f3bf64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c84d41a-552a-4883-bdba-772a4e7ba5d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}