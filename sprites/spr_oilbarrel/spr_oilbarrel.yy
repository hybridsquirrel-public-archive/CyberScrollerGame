{
    "id": "0e9c0219-2e86-4aa0-bf23-8f357f779aab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_oilbarrel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 10,
    "bbox_right": 28,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d96e27a7-1d8a-4be9-b043-e3a6ec3f75a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9c0219-2e86-4aa0-bf23-8f357f779aab",
            "compositeImage": {
                "id": "3bf7846c-eb09-4fd0-a1b6-f21d5eaf3558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d96e27a7-1d8a-4be9-b043-e3a6ec3f75a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692c59bd-7dc7-429b-9c2d-4c57e619e789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d96e27a7-1d8a-4be9-b043-e3a6ec3f75a8",
                    "LayerId": "29be4643-2c8c-4fac-a1e9-01179230c1fb"
                }
            ]
        },
        {
            "id": "8768426c-9c6a-40c9-9008-6ec61cc9ab4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9c0219-2e86-4aa0-bf23-8f357f779aab",
            "compositeImage": {
                "id": "c461d771-4335-4af5-93df-3c90c9f62cf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8768426c-9c6a-40c9-9008-6ec61cc9ab4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eac8a760-b021-4520-a378-5cbd40d703a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8768426c-9c6a-40c9-9008-6ec61cc9ab4b",
                    "LayerId": "29be4643-2c8c-4fac-a1e9-01179230c1fb"
                }
            ]
        },
        {
            "id": "b5da3512-20e1-4269-af5f-f648dc238868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9c0219-2e86-4aa0-bf23-8f357f779aab",
            "compositeImage": {
                "id": "ad246a70-6740-4371-bffb-c43034de000f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5da3512-20e1-4269-af5f-f648dc238868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "430f77eb-21b0-4ae2-86ea-9beaa160913d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5da3512-20e1-4269-af5f-f648dc238868",
                    "LayerId": "29be4643-2c8c-4fac-a1e9-01179230c1fb"
                }
            ]
        },
        {
            "id": "c34b8a03-ab17-492e-8e94-72c47975a663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9c0219-2e86-4aa0-bf23-8f357f779aab",
            "compositeImage": {
                "id": "70cc9167-c8d7-4f62-9b60-32f83c83348d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c34b8a03-ab17-492e-8e94-72c47975a663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d66e8a5-4f50-42b4-8166-4808bf067a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c34b8a03-ab17-492e-8e94-72c47975a663",
                    "LayerId": "29be4643-2c8c-4fac-a1e9-01179230c1fb"
                }
            ]
        },
        {
            "id": "dec8fed5-0d6f-4555-ae97-7ca2e0802b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9c0219-2e86-4aa0-bf23-8f357f779aab",
            "compositeImage": {
                "id": "5f0413a3-3724-4a30-8691-c97bc7da14c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec8fed5-0d6f-4555-ae97-7ca2e0802b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a339109-7b32-4307-a19e-a55a7375b5c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec8fed5-0d6f-4555-ae97-7ca2e0802b84",
                    "LayerId": "29be4643-2c8c-4fac-a1e9-01179230c1fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "29be4643-2c8c-4fac-a1e9-01179230c1fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e9c0219-2e86-4aa0-bf23-8f357f779aab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 19,
    "yorig": 20
}