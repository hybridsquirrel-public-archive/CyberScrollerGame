{
    "id": "04c498bd-7687-4fd9-a908-6fb31aa857e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 9,
    "bbox_right": 31,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fbbb11a-2116-456c-b5ba-121ce7e9541e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04c498bd-7687-4fd9-a908-6fb31aa857e7",
            "compositeImage": {
                "id": "45cea556-280a-4b7d-8161-4c3bab2caf4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fbbb11a-2116-456c-b5ba-121ce7e9541e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba7d0db4-8290-4e74-aff2-cf58114d73cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fbbb11a-2116-456c-b5ba-121ce7e9541e",
                    "LayerId": "123db382-82e2-4a0b-aea2-3ec317667732"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "123db382-82e2-4a0b-aea2-3ec317667732",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04c498bd-7687-4fd9-a908-6fb31aa857e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 18,
    "yorig": 30
}