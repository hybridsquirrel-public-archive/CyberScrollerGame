{
    "id": "61270b0f-458b-4e5e-92f0-1e0946ded120",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 12,
    "bbox_right": 267,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97b182f3-ef54-4170-9f67-4a202af0c965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61270b0f-458b-4e5e-92f0-1e0946ded120",
            "compositeImage": {
                "id": "f253ea6d-40fc-4aa8-9f76-730502fac50c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97b182f3-ef54-4170-9f67-4a202af0c965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a4a7e1-cd2c-46b4-9cb6-280e35263117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97b182f3-ef54-4170-9f67-4a202af0c965",
                    "LayerId": "260ff39b-4d91-465f-bba8-f2cfe45c4469"
                }
            ]
        },
        {
            "id": "11466236-e4ba-4297-b9f2-ad3ee30c10ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61270b0f-458b-4e5e-92f0-1e0946ded120",
            "compositeImage": {
                "id": "6a9f2b71-99d5-4235-85b8-2ea7085b3b50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11466236-e4ba-4297-b9f2-ad3ee30c10ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "065454ad-99b6-4483-8632-153ae12e2def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11466236-e4ba-4297-b9f2-ad3ee30c10ad",
                    "LayerId": "260ff39b-4d91-465f-bba8-f2cfe45c4469"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "260ff39b-4d91-465f-bba8-f2cfe45c4469",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61270b0f-458b-4e5e-92f0-1e0946ded120",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 273,
    "xorig": 136,
    "yorig": 135
}