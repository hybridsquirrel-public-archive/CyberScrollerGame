{
    "id": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b477811-69be-48ab-9013-c2fdd334b63a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "e6ac0f16-2150-4758-87d3-f5e7c219c209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b477811-69be-48ab-9013-c2fdd334b63a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b3162f-6598-405f-960f-9e2cae701471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b477811-69be-48ab-9013-c2fdd334b63a",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "eca442ca-c152-4b0c-a65c-352c165f9a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "ff5e6b46-0ce3-403b-b493-43f71a458424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eca442ca-c152-4b0c-a65c-352c165f9a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "735183dd-3514-4ee9-a8a9-08a1bbf7aba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eca442ca-c152-4b0c-a65c-352c165f9a4c",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "e9d740ab-6595-4f4f-bbc1-73f8f18a1cce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "c74f2607-5c8f-4f6a-8fa1-8c918ac901db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d740ab-6595-4f4f-bbc1-73f8f18a1cce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b94072cf-32e6-49fd-b3b7-73f3f9f33ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d740ab-6595-4f4f-bbc1-73f8f18a1cce",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "788889f4-2c34-4d9d-91c2-336235a845a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "e37f93b2-e440-4d9b-807f-755c7d56730f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "788889f4-2c34-4d9d-91c2-336235a845a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90ef6c06-8ad6-4664-ab4a-b8f5696a7b0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "788889f4-2c34-4d9d-91c2-336235a845a5",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "ccfaeb57-439c-4bf5-a1b7-a76264f2b703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "21a27479-4600-4084-91e3-44a07c3cad31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccfaeb57-439c-4bf5-a1b7-a76264f2b703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f81dd6c-d0e0-4c2c-b2c2-d7d3682882ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccfaeb57-439c-4bf5-a1b7-a76264f2b703",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "c4d2de1b-a619-4446-a536-b82e422ebaea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "6b33cb25-2abe-42d2-a8a2-970fcb880732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4d2de1b-a619-4446-a536-b82e422ebaea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9461c525-92bb-4ab5-97b4-a837fc7cdb7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4d2de1b-a619-4446-a536-b82e422ebaea",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "eeb72e54-b4c8-4be2-b08d-5a1844e1d604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "45ac6599-9860-492a-bc84-8485c22973cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeb72e54-b4c8-4be2-b08d-5a1844e1d604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dedb9a1-6326-4a72-bc86-4716c8da040c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeb72e54-b4c8-4be2-b08d-5a1844e1d604",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "131b43a9-f86c-494b-974a-a17173d163b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "316cb47a-1954-49f0-9ba0-bf98d0c63892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "131b43a9-f86c-494b-974a-a17173d163b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "306023d0-16e3-4df2-a190-31773f3da795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "131b43a9-f86c-494b-974a-a17173d163b7",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "9d67108b-840c-444a-a09f-9e7635a62e19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "8c8a5855-6a87-4529-8101-1017f2383a86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d67108b-840c-444a-a09f-9e7635a62e19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d67e8467-1b12-4a91-bb05-e3df677b3722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d67108b-840c-444a-a09f-9e7635a62e19",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        },
        {
            "id": "2038b952-57a6-458d-8447-2e3d75c5d70d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "compositeImage": {
                "id": "baec8eb3-24f8-4d2c-88e8-4abf1abd5468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2038b952-57a6-458d-8447-2e3d75c5d70d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81d6ac6a-bea3-4115-b67f-3d81b6dd361c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2038b952-57a6-458d-8447-2e3d75c5d70d",
                    "LayerId": "d232b228-9a08-45a5-9033-72c8198aa7fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "d232b228-9a08-45a5-9033-72c8198aa7fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 13
}