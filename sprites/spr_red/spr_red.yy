{
    "id": "54466562-d8b0-40f2-9853-170ae271f25f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 272,
    "bbox_left": 7,
    "bbox_right": 264,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01011baf-cb6d-4cf9-b0aa-aee1009c3731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54466562-d8b0-40f2-9853-170ae271f25f",
            "compositeImage": {
                "id": "1f42aa41-f8ee-4319-8f5d-4bb59aa58d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01011baf-cb6d-4cf9-b0aa-aee1009c3731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89a6c2d2-c3dd-423a-ac59-6339ce82a84b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01011baf-cb6d-4cf9-b0aa-aee1009c3731",
                    "LayerId": "7667ec7a-a849-4448-9b48-116905c9f97b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 273,
    "layers": [
        {
            "id": "7667ec7a-a849-4448-9b48-116905c9f97b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54466562-d8b0-40f2-9853-170ae271f25f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 265,
    "xorig": 132,
    "yorig": 136
}