{
    "id": "ba02dae6-f512-4588-ab90-5479a65feee9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skilltree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 351,
    "bbox_left": 17,
    "bbox_right": 504,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "468162a5-bc51-43bc-bd00-b96c6de5467b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba02dae6-f512-4588-ab90-5479a65feee9",
            "compositeImage": {
                "id": "ada5bb6b-6cc3-424e-b5bc-5ea155e173f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "468162a5-bc51-43bc-bd00-b96c6de5467b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ed9540-430e-4745-b6ea-254b99374610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "468162a5-bc51-43bc-bd00-b96c6de5467b",
                    "LayerId": "e74ed447-c35d-4dd5-9678-0b621e1d515f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 352,
    "layers": [
        {
            "id": "e74ed447-c35d-4dd5-9678-0b621e1d515f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba02dae6-f512-4588-ab90-5479a65feee9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 505,
    "xorig": 252,
    "yorig": 176
}