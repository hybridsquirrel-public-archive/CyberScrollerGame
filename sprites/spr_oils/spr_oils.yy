{
    "id": "fb5f0e54-7bef-42b9-a3dd-a3ecd9f9c98f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_oils",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 17,
    "bbox_right": 47,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "657ade0c-a710-4b04-b531-4e05b51310cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb5f0e54-7bef-42b9-a3dd-a3ecd9f9c98f",
            "compositeImage": {
                "id": "84343e33-776f-45a3-9391-fb93a444c43b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "657ade0c-a710-4b04-b531-4e05b51310cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e282ae0b-f7b0-4392-8c4a-7be80f9bd414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "657ade0c-a710-4b04-b531-4e05b51310cf",
                    "LayerId": "ec4af055-bdf4-4e08-b11e-67ab8048eeef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ec4af055-bdf4-4e08-b11e-67ab8048eeef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb5f0e54-7bef-42b9-a3dd-a3ecd9f9c98f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}