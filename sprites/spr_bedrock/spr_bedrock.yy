{
    "id": "40bf644d-5517-43a4-bc32-13d11dd35987",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bedrock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d24b2842-5a7d-447a-94d1-dd61fc2e614f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40bf644d-5517-43a4-bc32-13d11dd35987",
            "compositeImage": {
                "id": "64a59023-860b-4636-ac04-7ec152a44c0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d24b2842-5a7d-447a-94d1-dd61fc2e614f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "924cbf0c-2a74-4003-96e0-258d9ad5e7f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d24b2842-5a7d-447a-94d1-dd61fc2e614f",
                    "LayerId": "70426213-2432-4e79-9eb7-e9a2b75b65b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "70426213-2432-4e79-9eb7-e9a2b75b65b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40bf644d-5517-43a4-bc32-13d11dd35987",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}