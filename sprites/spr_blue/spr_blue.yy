{
    "id": "592af05c-1095-43ea-a868-fb589bad27dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 3,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "beb2bd98-83be-4aaf-bbab-e85eb62b8f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "592af05c-1095-43ea-a868-fb589bad27dd",
            "compositeImage": {
                "id": "d3df60b8-907c-4c71-ba1d-1f9b6e860995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beb2bd98-83be-4aaf-bbab-e85eb62b8f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68e3bb78-8261-4305-92a8-d37cb140e23e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beb2bd98-83be-4aaf-bbab-e85eb62b8f72",
                    "LayerId": "1cc172d8-bdf5-4c13-9a38-511b25fe418d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 251,
    "layers": [
        {
            "id": "1cc172d8-bdf5-4c13-9a38-511b25fe418d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "592af05c-1095-43ea-a868-fb589bad27dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 125
}