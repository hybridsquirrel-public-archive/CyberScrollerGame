{
    "id": "d8a304a0-09c1-4ea1-94c4-48719cd0cfbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_upgradebuttom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 266,
    "bbox_left": 10,
    "bbox_right": 266,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d16dbf1-d9dc-48be-bf54-7219120b876c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a304a0-09c1-4ea1-94c4-48719cd0cfbe",
            "compositeImage": {
                "id": "ad355cf1-e9e3-4350-93a2-9049fbf759fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d16dbf1-d9dc-48be-bf54-7219120b876c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5238a5d-7591-4c8a-b5bf-e766743bb5ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d16dbf1-d9dc-48be-bf54-7219120b876c",
                    "LayerId": "96605342-f7a0-4a39-b9bc-911698e0dee6"
                }
            ]
        },
        {
            "id": "deaccaac-9f26-4be6-84b1-5bcaac8861a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a304a0-09c1-4ea1-94c4-48719cd0cfbe",
            "compositeImage": {
                "id": "4b6d3bfd-bf2e-4bea-aa47-e92065bd7557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deaccaac-9f26-4be6-84b1-5bcaac8861a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db0360e5-d45e-4106-a595-768a501cb2fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deaccaac-9f26-4be6-84b1-5bcaac8861a1",
                    "LayerId": "96605342-f7a0-4a39-b9bc-911698e0dee6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "96605342-f7a0-4a39-b9bc-911698e0dee6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8a304a0-09c1-4ea1-94c4-48719cd0cfbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 274,
    "xorig": 137,
    "yorig": 135
}