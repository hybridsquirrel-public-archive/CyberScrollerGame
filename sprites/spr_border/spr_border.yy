{
    "id": "918d86e9-3796-4df0-8f00-ad7211058e01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b48b9772-12fd-4e4a-bcce-246dcfad9465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918d86e9-3796-4df0-8f00-ad7211058e01",
            "compositeImage": {
                "id": "61974f88-e4d3-4479-8ace-87f9b4840182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48b9772-12fd-4e4a-bcce-246dcfad9465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4aa33a3-d16f-4079-a394-adbefe834245",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48b9772-12fd-4e4a-bcce-246dcfad9465",
                    "LayerId": "83062499-5f07-4e6e-9c65-0fd22a58bf1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "83062499-5f07-4e6e-9c65-0fd22a58bf1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "918d86e9-3796-4df0-8f00-ad7211058e01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}