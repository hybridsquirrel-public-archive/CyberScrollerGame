{
    "id": "a791fda1-9298-4f6f-b5a3-7cd57bafc406",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_invetorygui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb97fbe5-578a-491f-9f42-1aca3cd4ce96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a791fda1-9298-4f6f-b5a3-7cd57bafc406",
            "compositeImage": {
                "id": "a2f7fdf6-578b-40b7-9f03-1313af342893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb97fbe5-578a-491f-9f42-1aca3cd4ce96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bd4482a-29b1-46c4-b227-7cf877b116a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb97fbe5-578a-491f-9f42-1aca3cd4ce96",
                    "LayerId": "359d12de-27ef-4ef9-bcc0-6eaf26796cb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "359d12de-27ef-4ef9-bcc0-6eaf26796cb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a791fda1-9298-4f6f-b5a3-7cd57bafc406",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 540
}