{
    "id": "70e4b559-c491-45b2-b43e-ef6ee6033258",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 39,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0bf5c19-cd74-457b-a6e0-01cfcb6cfa7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70e4b559-c491-45b2-b43e-ef6ee6033258",
            "compositeImage": {
                "id": "788876b5-15b5-48c5-84c2-887d697f83d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0bf5c19-cd74-457b-a6e0-01cfcb6cfa7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeb6d973-e7a2-482c-8b3f-886d7317e374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0bf5c19-cd74-457b-a6e0-01cfcb6cfa7b",
                    "LayerId": "09d10b83-2e27-47ba-ac93-f64f47fcac09"
                }
            ]
        },
        {
            "id": "0defc735-48c5-4fe0-97de-2c18ef7dcf25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70e4b559-c491-45b2-b43e-ef6ee6033258",
            "compositeImage": {
                "id": "d8f45dc4-b299-488e-b7a6-9da3ec0ae8d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0defc735-48c5-4fe0-97de-2c18ef7dcf25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b742a13-e3d9-4264-b0bb-96f27eac9b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0defc735-48c5-4fe0-97de-2c18ef7dcf25",
                    "LayerId": "09d10b83-2e27-47ba-ac93-f64f47fcac09"
                }
            ]
        },
        {
            "id": "ed03e6a1-447f-4cfd-b1e0-15035392443a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70e4b559-c491-45b2-b43e-ef6ee6033258",
            "compositeImage": {
                "id": "800dcbab-d5f4-422a-9ab2-89cde9857840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed03e6a1-447f-4cfd-b1e0-15035392443a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17362db9-0c0e-4765-81e5-8fdf60312769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed03e6a1-447f-4cfd-b1e0-15035392443a",
                    "LayerId": "09d10b83-2e27-47ba-ac93-f64f47fcac09"
                }
            ]
        },
        {
            "id": "5c0ab4dc-960f-44cd-b133-885853d6e40e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70e4b559-c491-45b2-b43e-ef6ee6033258",
            "compositeImage": {
                "id": "96bc5acc-9de4-450d-819f-ded184d0acb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c0ab4dc-960f-44cd-b133-885853d6e40e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8fb1643-9c0d-48b4-b716-0d2052e052ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0ab4dc-960f-44cd-b133-885853d6e40e",
                    "LayerId": "09d10b83-2e27-47ba-ac93-f64f47fcac09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "09d10b83-2e27-47ba-ac93-f64f47fcac09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70e4b559-c491-45b2-b43e-ef6ee6033258",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 17,
    "yorig": 31
}