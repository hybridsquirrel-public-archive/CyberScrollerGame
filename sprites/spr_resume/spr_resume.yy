{
    "id": "84358385-0535-4191-9f75-cbd173f07203",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_resume",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 262,
    "bbox_left": 6,
    "bbox_right": 261,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4929fa23-9ef7-4080-a0c8-dce8a59a8c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84358385-0535-4191-9f75-cbd173f07203",
            "compositeImage": {
                "id": "8894c143-0cf5-4836-bfaf-26267556ba4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4929fa23-9ef7-4080-a0c8-dce8a59a8c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c8d5978-ec3c-4756-b08f-815819f73d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4929fa23-9ef7-4080-a0c8-dce8a59a8c03",
                    "LayerId": "f3a8fef9-6299-455b-81f3-17aea02ee3f4"
                }
            ]
        },
        {
            "id": "14bff525-0e4b-479b-a1dc-8756fa396fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84358385-0535-4191-9f75-cbd173f07203",
            "compositeImage": {
                "id": "6e091c2f-84ac-48b8-9de9-694295962394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14bff525-0e4b-479b-a1dc-8756fa396fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "365d1641-4dff-4deb-8265-d5adcd6cb43e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14bff525-0e4b-479b-a1dc-8756fa396fbf",
                    "LayerId": "f3a8fef9-6299-455b-81f3-17aea02ee3f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 272,
    "layers": [
        {
            "id": "f3a8fef9-6299-455b-81f3-17aea02ee3f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84358385-0535-4191-9f75-cbd173f07203",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 270,
    "xorig": 135,
    "yorig": 136
}