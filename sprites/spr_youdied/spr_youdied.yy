{
    "id": "7d31a2e6-f7da-4cbe-a1bc-78a6afc899df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_youdied",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6ed9322-d126-4978-a1d0-a4cff862d932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d31a2e6-f7da-4cbe-a1bc-78a6afc899df",
            "compositeImage": {
                "id": "9fc393f2-cfcc-4c93-a95c-0ec48acf91dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6ed9322-d126-4978-a1d0-a4cff862d932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3abbf8fb-05bd-4ce8-a99e-d5edcb1931fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6ed9322-d126-4978-a1d0-a4cff862d932",
                    "LayerId": "b29ae05f-79de-4ba2-b31b-e837d0d87dcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "b29ae05f-79de-4ba2-b31b-e837d0d87dcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d31a2e6-f7da-4cbe-a1bc-78a6afc899df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}