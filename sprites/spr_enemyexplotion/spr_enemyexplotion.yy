{
    "id": "0b9b06c9-652e-4698-b7ed-28fb658f0235",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyexplotion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "777993a8-b3b4-406c-96a9-7e9cbafe5b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9b06c9-652e-4698-b7ed-28fb658f0235",
            "compositeImage": {
                "id": "a3df2081-3b96-44cd-a950-714cd25f21f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "777993a8-b3b4-406c-96a9-7e9cbafe5b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "045c38e0-4734-4ef1-9007-31dd4cf002f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "777993a8-b3b4-406c-96a9-7e9cbafe5b94",
                    "LayerId": "611c6186-0953-4182-a6e5-0c87203e7adb"
                }
            ]
        },
        {
            "id": "938ee632-d656-4da0-a278-61ca80f9dc6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9b06c9-652e-4698-b7ed-28fb658f0235",
            "compositeImage": {
                "id": "d29faa53-6637-4890-86d1-7c61da69f0f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "938ee632-d656-4da0-a278-61ca80f9dc6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b4b6ca3-fe77-4ceb-adfd-a4a86fade68b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "938ee632-d656-4da0-a278-61ca80f9dc6f",
                    "LayerId": "611c6186-0953-4182-a6e5-0c87203e7adb"
                }
            ]
        },
        {
            "id": "935d0652-24e5-49c2-92cc-465a92640ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9b06c9-652e-4698-b7ed-28fb658f0235",
            "compositeImage": {
                "id": "d0a3bb22-3609-4b57-b88e-6c9fa374ad74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935d0652-24e5-49c2-92cc-465a92640ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bab5143-6201-442f-8ca8-f50eecc6d565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935d0652-24e5-49c2-92cc-465a92640ca0",
                    "LayerId": "611c6186-0953-4182-a6e5-0c87203e7adb"
                }
            ]
        },
        {
            "id": "72156e04-061c-47be-9928-cf9423148805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9b06c9-652e-4698-b7ed-28fb658f0235",
            "compositeImage": {
                "id": "656b5bce-ffa1-4069-b001-c85b7037a9fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72156e04-061c-47be-9928-cf9423148805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "437648bd-5886-4061-8a7e-8c3e2a6083bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72156e04-061c-47be-9928-cf9423148805",
                    "LayerId": "611c6186-0953-4182-a6e5-0c87203e7adb"
                }
            ]
        },
        {
            "id": "3507b415-19ff-4f57-afab-b9447eff617e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9b06c9-652e-4698-b7ed-28fb658f0235",
            "compositeImage": {
                "id": "0383c5a3-e32e-4545-8af0-b7dc13508066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3507b415-19ff-4f57-afab-b9447eff617e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "447ee635-28da-43ca-b30b-0deeb63369d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3507b415-19ff-4f57-afab-b9447eff617e",
                    "LayerId": "611c6186-0953-4182-a6e5-0c87203e7adb"
                }
            ]
        },
        {
            "id": "831bb91c-a54e-4464-8520-1a56740e4ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9b06c9-652e-4698-b7ed-28fb658f0235",
            "compositeImage": {
                "id": "79ccef96-a722-4428-a19d-11e12c87bf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "831bb91c-a54e-4464-8520-1a56740e4ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0d181e-392b-4c3f-8f7b-1f88dae3c054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "831bb91c-a54e-4464-8520-1a56740e4ecc",
                    "LayerId": "611c6186-0953-4182-a6e5-0c87203e7adb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "611c6186-0953-4182-a6e5-0c87203e7adb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b9b06c9-652e-4698-b7ed-28fb658f0235",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}