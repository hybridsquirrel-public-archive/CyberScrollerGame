{
    "id": "4c7bbc26-419f-44f0-b997-8a16d11733cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_text",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 11,
    "bbox_right": 654,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d079224-eaa1-467f-b827-eb44324b95ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c7bbc26-419f-44f0-b997-8a16d11733cf",
            "compositeImage": {
                "id": "ba55476d-88ad-42b8-a616-08505f6622c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d079224-eaa1-467f-b827-eb44324b95ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c468bf75-0c66-4630-8fa5-1330c49f60a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d079224-eaa1-467f-b827-eb44324b95ae",
                    "LayerId": "9e998b43-2166-4f5f-9e26-6e9455422950"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "9e998b43-2166-4f5f-9e26-6e9455422950",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c7bbc26-419f-44f0-b997-8a16d11733cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 663,
    "xorig": 331,
    "yorig": 23
}