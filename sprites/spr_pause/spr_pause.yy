{
    "id": "e35cf593-0d8f-46d4-8042-dbb2e814db4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c786eb5-025e-474e-8f1e-430ea8243ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e35cf593-0d8f-46d4-8042-dbb2e814db4d",
            "compositeImage": {
                "id": "6c7231ca-deaf-4608-bb15-638cf8fe4a01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c786eb5-025e-474e-8f1e-430ea8243ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c32d95-cf3a-4931-899d-dae7570a761a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c786eb5-025e-474e-8f1e-430ea8243ca7",
                    "LayerId": "85004cae-5ad3-49fd-bcc4-f4d2e5600f0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "85004cae-5ad3-49fd-bcc4-f4d2e5600f0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e35cf593-0d8f-46d4-8042-dbb2e814db4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}