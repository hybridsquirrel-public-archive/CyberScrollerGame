{
    "id": "bc0cac84-fc0c-4122-aa01-d23e09652a88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backgroundsky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cb4601c-6747-4e73-afe7-efceb6831316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc0cac84-fc0c-4122-aa01-d23e09652a88",
            "compositeImage": {
                "id": "96a97d4e-ba24-4b1e-a25b-dcf71d807cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb4601c-6747-4e73-afe7-efceb6831316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b56aa369-1ccb-4a8a-850b-4e504084ef6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb4601c-6747-4e73-afe7-efceb6831316",
                    "LayerId": "ad7e7b74-77b7-41d9-9d60-fb274c894e48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "ad7e7b74-77b7-41d9-9d60-fb274c894e48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc0cac84-fc0c-4122-aa01-d23e09652a88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}