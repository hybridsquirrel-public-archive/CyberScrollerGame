{
    "id": "c4f9a150-342a-48f9-a4f3-fe7f3665b085",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_metal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 13,
    "bbox_right": 50,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd00a2db-d100-4518-971c-fa8cbbf5409e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f9a150-342a-48f9-a4f3-fe7f3665b085",
            "compositeImage": {
                "id": "dd40a0dd-2713-4662-a654-9dd126fca8b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd00a2db-d100-4518-971c-fa8cbbf5409e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "977e6ab8-4d9e-4ad7-8f90-5d09cb192ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd00a2db-d100-4518-971c-fa8cbbf5409e",
                    "LayerId": "d83b7e3d-c5c2-4232-a606-831aa66a77aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d83b7e3d-c5c2-4232-a606-831aa66a77aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4f9a150-342a-48f9-a4f3-fe7f3665b085",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}