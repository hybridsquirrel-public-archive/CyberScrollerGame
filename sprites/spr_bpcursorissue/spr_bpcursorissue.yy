{
    "id": "69805afb-fc59-4505-ad9b-dd738d256b16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bpcursorissue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0de58d71-d8b5-42ce-a1e0-4cc9c612176d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69805afb-fc59-4505-ad9b-dd738d256b16",
            "compositeImage": {
                "id": "15c640c8-7feb-441b-a572-e44e3fc54c7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de58d71-d8b5-42ce-a1e0-4cc9c612176d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e032601b-89e8-45e9-a8a9-b6b7385c0780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de58d71-d8b5-42ce-a1e0-4cc9c612176d",
                    "LayerId": "c2701270-0d2b-425c-918d-89a79c6c861c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c2701270-0d2b-425c-918d-89a79c6c861c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69805afb-fc59-4505-ad9b-dd738d256b16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}