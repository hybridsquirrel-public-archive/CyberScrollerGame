{
    "id": "55121ed5-c3a9-4bef-8bef-11b36b7b69f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_howplay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 265,
    "bbox_left": 19,
    "bbox_right": 274,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24f1970d-6c01-484e-8898-1958497019f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55121ed5-c3a9-4bef-8bef-11b36b7b69f9",
            "compositeImage": {
                "id": "eef0f372-1489-4cfd-b034-b4f211d64692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24f1970d-6c01-484e-8898-1958497019f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e890eb6-fc10-4913-82f7-04f411958f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24f1970d-6c01-484e-8898-1958497019f6",
                    "LayerId": "5ac67f85-2fb9-4074-8379-aaf6ae56b9b3"
                }
            ]
        },
        {
            "id": "d113a10b-80f9-4616-ae06-a88781ed4372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55121ed5-c3a9-4bef-8bef-11b36b7b69f9",
            "compositeImage": {
                "id": "36ff5cf6-59fc-4a0c-89b0-3fb5ec03d9ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d113a10b-80f9-4616-ae06-a88781ed4372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "966bed7b-d73b-4355-8680-1e2a9d7a916e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d113a10b-80f9-4616-ae06-a88781ed4372",
                    "LayerId": "5ac67f85-2fb9-4074-8379-aaf6ae56b9b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "5ac67f85-2fb9-4074-8379-aaf6ae56b9b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55121ed5-c3a9-4bef-8bef-11b36b7b69f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 286,
    "xorig": 143,
    "yorig": 135
}