{
    "id": "25dd8000-d712-4e01-9a9e-ff32e9797146",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darkbrik",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "252d9e68-39ac-4306-aa73-b02fe0c11c9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25dd8000-d712-4e01-9a9e-ff32e9797146",
            "compositeImage": {
                "id": "3d219525-5f53-4c00-859e-34324cceb6de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "252d9e68-39ac-4306-aa73-b02fe0c11c9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a59c2fb-5b49-402b-82e8-8a4a4c706ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "252d9e68-39ac-4306-aa73-b02fe0c11c9a",
                    "LayerId": "500fa35a-624d-4497-842e-97e0a82da9d4"
                }
            ]
        },
        {
            "id": "826bd70e-0cd0-4ea4-906f-e3a36b574cb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25dd8000-d712-4e01-9a9e-ff32e9797146",
            "compositeImage": {
                "id": "c19c2b34-c4f7-4b13-8e67-d606c918c298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "826bd70e-0cd0-4ea4-906f-e3a36b574cb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e551a124-2227-4c46-b85b-dff85a484818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "826bd70e-0cd0-4ea4-906f-e3a36b574cb0",
                    "LayerId": "500fa35a-624d-4497-842e-97e0a82da9d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "500fa35a-624d-4497-842e-97e0a82da9d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25dd8000-d712-4e01-9a9e-ff32e9797146",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}