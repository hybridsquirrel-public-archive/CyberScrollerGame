{
    "id": "d374e982-0771-4da6-99c9-7f9c475df247",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_unlockbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3bf5a7d-5432-4fda-9114-4cda35928a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "b2cc3444-cd83-47d3-96cb-67914b206829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3bf5a7d-5432-4fda-9114-4cda35928a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e288aa1a-a6cb-48ef-82a4-91a5192210c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3bf5a7d-5432-4fda-9114-4cda35928a87",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "a017193f-6506-43b3-9669-1a1ff534f83b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "4b7e3b6f-f523-4341-8255-6cd185d55fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a017193f-6506-43b3-9669-1a1ff534f83b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d9b011d-40ab-4207-8828-4d6073ca6f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a017193f-6506-43b3-9669-1a1ff534f83b",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "b0b07919-ef66-401e-b5f3-bccd772f91a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "f4ade78a-0819-4ae7-8bee-f78d6377a305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0b07919-ef66-401e-b5f3-bccd772f91a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63a5b891-6b5e-417e-8c9c-0f3edebf9848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0b07919-ef66-401e-b5f3-bccd772f91a5",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "6a65db17-bc36-4383-b5cc-f1284f028218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "047ccfc2-6ba5-44ea-9ba9-a90658a377d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a65db17-bc36-4383-b5cc-f1284f028218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d895d2-ee87-431f-82a2-78d0c25488bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a65db17-bc36-4383-b5cc-f1284f028218",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "319ed958-4e49-4e74-9032-52b0f3d3b0ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "9fd94495-018f-450f-ad3a-e764e61127eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "319ed958-4e49-4e74-9032-52b0f3d3b0ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4c731d-7760-4e98-8497-55ced34743c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "319ed958-4e49-4e74-9032-52b0f3d3b0ca",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "4b9a3007-928e-4d7d-a0d4-7a306278e815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "d544dbc5-f57e-4464-9e05-2b2e347f62de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b9a3007-928e-4d7d-a0d4-7a306278e815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79735c6d-f65d-4d58-8078-41a33674a2c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b9a3007-928e-4d7d-a0d4-7a306278e815",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "bf42afc4-2d04-4ed8-b5a4-4686b009c3aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "12f647f4-2c9e-4ca4-a0cf-77496a5bebc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf42afc4-2d04-4ed8-b5a4-4686b009c3aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8388953f-705d-411b-9a04-3d15f42d0064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf42afc4-2d04-4ed8-b5a4-4686b009c3aa",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "e431bd12-018b-479c-8e4b-722d54f403ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "d14706bc-6635-4030-8d28-1803ec1f6cd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e431bd12-018b-479c-8e4b-722d54f403ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c53fe0a-7e93-4792-878b-44cbb139a71a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e431bd12-018b-479c-8e4b-722d54f403ab",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "0c4861ee-9b95-4f4a-98a7-94a3e43e85be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "c8647649-880f-4244-9b57-8291865bacc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c4861ee-9b95-4f4a-98a7-94a3e43e85be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6271d8-36b7-41b4-ad6a-8dde35fd57b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c4861ee-9b95-4f4a-98a7-94a3e43e85be",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "0c21cdba-5988-4e80-98b1-8b317b88e362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "cc520506-ad38-48ca-a17c-4f9f8db3977d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c21cdba-5988-4e80-98b1-8b317b88e362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2789baf1-5c16-47ba-b8be-afba3920b772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c21cdba-5988-4e80-98b1-8b317b88e362",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "823b1a2c-cda0-4cf5-83fa-d040cd5be368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "f0fe7e98-6e0a-46e2-bcdc-011a5df71cef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823b1a2c-cda0-4cf5-83fa-d040cd5be368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf6b1706-f10b-4027-8f78-bec6911791d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823b1a2c-cda0-4cf5-83fa-d040cd5be368",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "1f6213c9-35b8-41ef-b92e-4cd20d83e9a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "e6182194-307c-437f-b7c2-a2c2d6eaca93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f6213c9-35b8-41ef-b92e-4cd20d83e9a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c32ebf-cbed-4553-8bff-ef3dbe6a15f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f6213c9-35b8-41ef-b92e-4cd20d83e9a6",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "c01b5769-7227-47ae-b8f5-3e54197c9d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "359e818c-bcd8-4cb3-aa36-1a1340a5ac0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c01b5769-7227-47ae-b8f5-3e54197c9d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2197ddb1-b859-4b99-9683-3519bd40ad69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c01b5769-7227-47ae-b8f5-3e54197c9d23",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "a91f2a80-404b-447e-8d86-1e3675a7c3a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "51e20c22-c30a-469e-a957-2830bd5d43cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a91f2a80-404b-447e-8d86-1e3675a7c3a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf2119c-06a5-424a-85d5-94abe48ac319",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a91f2a80-404b-447e-8d86-1e3675a7c3a2",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        },
        {
            "id": "3e10ad3f-6e85-46ce-8c6b-ee8a01c0b6bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "compositeImage": {
                "id": "9a21ce4f-5ce2-4739-8f5f-0ccbd6bfdba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e10ad3f-6e85-46ce-8c6b-ee8a01c0b6bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b49312e7-5716-44b6-bff2-54ebe9e4336a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e10ad3f-6e85-46ce-8c6b-ee8a01c0b6bb",
                    "LayerId": "3cef30f2-eb56-4bc6-a035-fe759c03c99c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "3cef30f2-eb56-4bc6-a035-fe759c03c99c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d374e982-0771-4da6-99c9-7f9c475df247",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 40
}