{
    "id": "50209114-b7a5-4fc5-a25a-c7339076d2be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 475,
    "bbox_left": 55,
    "bbox_right": 351,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 129,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b894b441-f986-4ff6-ac98-bd4b4d93b4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50209114-b7a5-4fc5-a25a-c7339076d2be",
            "compositeImage": {
                "id": "bcb59003-35ad-45cc-b790-d8d012fc476b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b894b441-f986-4ff6-ac98-bd4b4d93b4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "633efcb9-7c51-4c33-bb6b-032dedba1b3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b894b441-f986-4ff6-ac98-bd4b4d93b4ef",
                    "LayerId": "452770a0-2969-4638-9644-1618b8db6eb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "452770a0-2969-4638-9644-1618b8db6eb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50209114-b7a5-4fc5-a25a-c7339076d2be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 200,
    "yorig": 250
}