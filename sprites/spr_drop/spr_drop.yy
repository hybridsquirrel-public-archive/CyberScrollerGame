{
    "id": "84aaa0cc-59ba-4844-8042-a8643902a7ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_drop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 1939,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91554597-c49a-43b8-aa47-8d6719d01303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84aaa0cc-59ba-4844-8042-a8643902a7ee",
            "compositeImage": {
                "id": "7e30f76f-2f54-424d-a301-4d1f4b80264d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91554597-c49a-43b8-aa47-8d6719d01303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba594ef-c6e7-48ee-b508-0546ce8f43b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91554597-c49a-43b8-aa47-8d6719d01303",
                    "LayerId": "b9ef2105-0f07-450f-ad09-3b15d0f28266"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b9ef2105-0f07-450f-ad09-3b15d0f28266",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84aaa0cc-59ba-4844-8042-a8643902a7ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1940,
    "xorig": 970,
    "yorig": 0
}