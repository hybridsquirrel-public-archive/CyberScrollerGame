{
    "id": "e4079490-20b9-419f-b93e-04ded71e6ec1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 2,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d665a994-4e1a-4f40-8791-c3ed0e2ddfec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4079490-20b9-419f-b93e-04ded71e6ec1",
            "compositeImage": {
                "id": "e7d36cba-5031-4e5c-99ee-4248f4bde9b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d665a994-4e1a-4f40-8791-c3ed0e2ddfec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9637fa0-b6fa-49ce-9131-750387817aa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d665a994-4e1a-4f40-8791-c3ed0e2ddfec",
                    "LayerId": "b25da564-a5da-4e51-9d88-38a09a584b01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "b25da564-a5da-4e51-9d88-38a09a584b01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4079490-20b9-419f-b93e-04ded71e6ec1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 15,
    "yorig": 22
}