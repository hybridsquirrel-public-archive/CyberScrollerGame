{
    "id": "e984e726-d7ab-4a61-b2db-d0946f1c5f21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barrelexpoltion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bccceb0-7f28-4579-b3ce-0c5b929279ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e984e726-d7ab-4a61-b2db-d0946f1c5f21",
            "compositeImage": {
                "id": "981a1a41-88da-4150-80fb-784c624b895f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bccceb0-7f28-4579-b3ce-0c5b929279ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604049df-2614-44d8-a38a-1fae5bde8282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bccceb0-7f28-4579-b3ce-0c5b929279ea",
                    "LayerId": "c58f5b2f-7108-42c5-badf-533be9a84a5f"
                }
            ]
        },
        {
            "id": "99829a95-2568-4d43-8fa2-0be1e8823a79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e984e726-d7ab-4a61-b2db-d0946f1c5f21",
            "compositeImage": {
                "id": "d02e1f53-cd51-41c9-80d0-1cc857dfc54a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99829a95-2568-4d43-8fa2-0be1e8823a79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e03ec23a-78cb-4143-868b-7da8bf38a928",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99829a95-2568-4d43-8fa2-0be1e8823a79",
                    "LayerId": "c58f5b2f-7108-42c5-badf-533be9a84a5f"
                }
            ]
        },
        {
            "id": "8e29bff4-2294-4ecb-bc95-791644590155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e984e726-d7ab-4a61-b2db-d0946f1c5f21",
            "compositeImage": {
                "id": "6fa897a2-44f5-46f9-ab70-9a1f4750cfd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e29bff4-2294-4ecb-bc95-791644590155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b3bfdb-3793-4549-b5b4-ff34cf2dfc05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e29bff4-2294-4ecb-bc95-791644590155",
                    "LayerId": "c58f5b2f-7108-42c5-badf-533be9a84a5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c58f5b2f-7108-42c5-badf-533be9a84a5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e984e726-d7ab-4a61-b2db-d0946f1c5f21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}