{
    "id": "75334a9a-98a8-4b70-bf13-06adbd011338",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 3,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 18,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b241eb0-06bf-42d2-ad16-4ae0dd3a5388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75334a9a-98a8-4b70-bf13-06adbd011338",
            "compositeImage": {
                "id": "502a5897-607d-45ca-8646-6881fb7a5e44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b241eb0-06bf-42d2-ad16-4ae0dd3a5388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44bda542-bf59-48af-b8b9-c34094c1e7f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b241eb0-06bf-42d2-ad16-4ae0dd3a5388",
                    "LayerId": "683c51ab-f5bb-4de9-a6bd-60e2ceecd5dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "683c51ab-f5bb-4de9-a6bd-60e2ceecd5dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75334a9a-98a8-4b70-bf13-06adbd011338",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}