{
    "id": "f3635109-652c-44d3-91a6-d97948a6efc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lifebar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 88,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c78a3aa-9429-4d20-bc24-caf362b6ee00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "80a72075-99d6-4d7c-8da3-ad1a09d3b6f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c78a3aa-9429-4d20-bc24-caf362b6ee00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c826638-bc21-4960-9270-da90a9f22e81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c78a3aa-9429-4d20-bc24-caf362b6ee00",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "aa5770ca-68ba-44c2-9bec-31582b8a5504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "6cbe7e7f-368a-4f95-b3db-7808540b2ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5770ca-68ba-44c2-9bec-31582b8a5504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfcd214-193f-4dde-9182-15ae4beeeed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5770ca-68ba-44c2-9bec-31582b8a5504",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "d237a96f-36b9-488f-a917-6dcd4b0532e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "008dee1d-b8e7-4daa-995e-249a97c7e92e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d237a96f-36b9-488f-a917-6dcd4b0532e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46ec0499-8b96-4497-aec1-9177882a2707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d237a96f-36b9-488f-a917-6dcd4b0532e8",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "07e1178b-76f6-422e-a913-3d92fe2567c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "0090c916-c9f8-4981-9ab4-61340923e0cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07e1178b-76f6-422e-a913-3d92fe2567c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ded9a2-feaf-4289-8bf3-07964be0ce18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07e1178b-76f6-422e-a913-3d92fe2567c7",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "5ce0aa11-18c7-4613-8cdd-09824d4629bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "49cea5e0-780b-40cd-a537-3b14c6a93ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce0aa11-18c7-4613-8cdd-09824d4629bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c46a8659-e155-44a4-8625-0c3a2564ee19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce0aa11-18c7-4613-8cdd-09824d4629bc",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "f6029d2b-d33f-4dce-a376-cb4b2b751203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "5b7c3b99-b095-45c8-bebf-d82ec1de4764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6029d2b-d33f-4dce-a376-cb4b2b751203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c2e538-408e-4842-a05a-1f98b40a0e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6029d2b-d33f-4dce-a376-cb4b2b751203",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "685d4e19-21a6-420b-a72b-d16c67c7db0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "9910b38b-8bb1-4c83-b316-c347b23e027d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685d4e19-21a6-420b-a72b-d16c67c7db0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66562fd-17f8-42f7-b2b3-a5c227442a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685d4e19-21a6-420b-a72b-d16c67c7db0a",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "01bd8a65-a8cc-4dee-ab60-7e054383ea5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "61789b7e-dbe5-40b8-9046-8ad175232bea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01bd8a65-a8cc-4dee-ab60-7e054383ea5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4da172db-5f24-4d9e-a63e-5b77cb9c355d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01bd8a65-a8cc-4dee-ab60-7e054383ea5c",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "279a8f10-c586-48cf-931d-92d51d4c44fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "d4a21e5b-52a1-48f9-9849-52af74e703ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "279a8f10-c586-48cf-931d-92d51d4c44fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf52a60d-595f-4cac-987e-5b231a6d6547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "279a8f10-c586-48cf-931d-92d51d4c44fe",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "51f94425-264b-40dc-80a3-d2f07d51ac67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "296310ff-ac5d-41dc-96c3-d27d1ef2fda5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51f94425-264b-40dc-80a3-d2f07d51ac67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b549d4-08cd-4207-a2aa-8d51f47f0494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51f94425-264b-40dc-80a3-d2f07d51ac67",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "7a54ac9a-0068-49df-acf3-b4f9015276bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "3dbe0bb4-f97c-4abb-93fb-930b80560edb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a54ac9a-0068-49df-acf3-b4f9015276bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa26b6e2-3010-4ea3-b0e4-bdc982caa480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a54ac9a-0068-49df-acf3-b4f9015276bc",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "9c239147-2b4b-4cf2-af43-702e999a9b41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "23f56b13-becc-4ae3-a432-b5acefa38b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c239147-2b4b-4cf2-af43-702e999a9b41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d6b136-ea63-49e1-a695-2b5c5f2c9f4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c239147-2b4b-4cf2-af43-702e999a9b41",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "e21db574-3f68-4c67-8f7e-bb39818026af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "8bcc5d5e-63a5-4d2b-88cc-2380cd7246d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e21db574-3f68-4c67-8f7e-bb39818026af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2491662-fdaa-474e-87bf-1582edffa05c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21db574-3f68-4c67-8f7e-bb39818026af",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "b9211303-1dc9-4bfa-be21-273a6cd345d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "2c1f1870-7670-450d-a787-5440b1aa58c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9211303-1dc9-4bfa-be21-273a6cd345d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc498967-3aa1-4a3e-be9c-13553f14f5d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9211303-1dc9-4bfa-be21-273a6cd345d0",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "ed9f316b-d0b4-4ac9-ba0d-d22a93e6d87d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "ac62066e-9bc0-4545-8ec1-5d2decd6095e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9f316b-d0b4-4ac9-ba0d-d22a93e6d87d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85ab3156-77dd-4991-abaf-7691db744ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9f316b-d0b4-4ac9-ba0d-d22a93e6d87d",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "1022d924-1dc0-44e1-9ccd-8c33acffebd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "4a1bcbae-383e-452f-af25-d527f2324ded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1022d924-1dc0-44e1-9ccd-8c33acffebd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f95440a4-ea1d-4ae8-bd7f-b5efd66e9531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1022d924-1dc0-44e1-9ccd-8c33acffebd7",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "38c5ea78-4629-4ea0-9aad-9274d22e3bd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "c699ae6c-1487-4ef3-910c-8a760665ded4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38c5ea78-4629-4ea0-9aad-9274d22e3bd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "191ff4fc-fb96-44fc-8176-9a0e62d5ae54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38c5ea78-4629-4ea0-9aad-9274d22e3bd5",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "dd72713e-07bd-4e55-b619-069ab469f0da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "f64184db-5809-4edc-b6bd-4a99faf6fef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd72713e-07bd-4e55-b619-069ab469f0da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774188de-b4e2-4c05-871a-6b805d6a2bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd72713e-07bd-4e55-b619-069ab469f0da",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "513701c8-6c20-44ee-b403-252abf7b2bcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "e3e05aac-7cb0-4a25-96e2-362e94edbd5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "513701c8-6c20-44ee-b403-252abf7b2bcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9aee114-97f4-45ac-a14d-c8fa6c487d52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "513701c8-6c20-44ee-b403-252abf7b2bcd",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "d3cea63e-17aa-4977-b84a-3be1f0e4778b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "cc28b3f2-afc8-4ac3-b440-b8be0de85b05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3cea63e-17aa-4977-b84a-3be1f0e4778b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2ac7603-7983-4390-9359-b5546394e896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3cea63e-17aa-4977-b84a-3be1f0e4778b",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "79587cd5-27ce-41e3-8528-b0e6ab09bf05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "bf442361-a6c5-41e8-8b95-6d9686680eb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79587cd5-27ce-41e3-8528-b0e6ab09bf05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cbe5562-4f47-4d1a-a50f-bf7bb57982bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79587cd5-27ce-41e3-8528-b0e6ab09bf05",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "b6d029ec-98e8-4689-9030-de066aee445f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "082818a9-2c67-4f08-8fea-42cc0882c7c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d029ec-98e8-4689-9030-de066aee445f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24435cc6-d47a-43ce-b5e6-5214ec249af4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d029ec-98e8-4689-9030-de066aee445f",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "1a9b56ac-d483-4c0a-9ad3-870280c48521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "c5ee19ac-d341-438c-b2d6-f93d8e6f8ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9b56ac-d483-4c0a-9ad3-870280c48521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e80363d2-ac2a-4693-aa4f-ec34d43148b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9b56ac-d483-4c0a-9ad3-870280c48521",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "03bbcb2a-6724-4617-9b25-1538a48baa1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "c85225f7-5e37-4c33-bca7-bf36d3f39571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03bbcb2a-6724-4617-9b25-1538a48baa1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a573b1fd-24d0-4915-82e1-b964a90b2149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03bbcb2a-6724-4617-9b25-1538a48baa1b",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "b5c1526f-31b0-41a0-bc65-101a652b0e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "c2fabd6c-5958-4dab-9427-b26f64c2f6c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c1526f-31b0-41a0-bc65-101a652b0e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e417bb8-6969-4ed7-9775-cc30e1de6ba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c1526f-31b0-41a0-bc65-101a652b0e6f",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "b0d4f18e-47ef-4d98-a0c2-8ce9cfa55eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "f9fd8241-216b-4e20-9439-c00108012afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0d4f18e-47ef-4d98-a0c2-8ce9cfa55eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce9f777a-d2af-4a11-a090-310d6186c9fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0d4f18e-47ef-4d98-a0c2-8ce9cfa55eb8",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "6e211f4b-95da-4b31-8b24-2846fa58fce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "75bc7c72-8666-4d1d-b133-c3a23cc8e7ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e211f4b-95da-4b31-8b24-2846fa58fce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ac8e06-a186-4ae3-b54c-dee122590858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e211f4b-95da-4b31-8b24-2846fa58fce9",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "5dd8d580-b784-4534-95cf-e8fdb11fcaed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "4c2d84fe-12a8-4c5a-8b40-b97a463432cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd8d580-b784-4534-95cf-e8fdb11fcaed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe3da77-c112-4072-a574-b7a4a0089b11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd8d580-b784-4534-95cf-e8fdb11fcaed",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "ba3d9bb0-6c61-49aa-9d3f-d96554f48705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "fb8e6bd5-5a39-47ce-a0eb-ae70d35a2486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba3d9bb0-6c61-49aa-9d3f-d96554f48705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2491ea4-6718-43c2-be9b-230008628410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba3d9bb0-6c61-49aa-9d3f-d96554f48705",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "951fd9cf-1265-4ee5-9148-49ff3ffce0e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "177b3ae5-4d57-462a-9e1d-c1d440c77424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "951fd9cf-1265-4ee5-9148-49ff3ffce0e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fda516a-1c9f-4b43-88f3-838d53c7f91d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "951fd9cf-1265-4ee5-9148-49ff3ffce0e5",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "9b4f9474-bfbf-4c94-88a7-2387275fa346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "32b62dfa-54dd-4749-b69a-39e6cf046562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b4f9474-bfbf-4c94-88a7-2387275fa346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d02043d-3b2c-4559-8cce-ba8ce843ae38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b4f9474-bfbf-4c94-88a7-2387275fa346",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "1a3e476c-f6ad-4171-9c0d-6cb5b9421eb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "4f3fcb8f-e6f3-4c23-9011-12a4ed544167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a3e476c-f6ad-4171-9c0d-6cb5b9421eb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1152cb21-e3e3-4338-a6e2-dc599224e3e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3e476c-f6ad-4171-9c0d-6cb5b9421eb7",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "91418d23-9fb0-48c4-be9f-4ce7c7564df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "cff17129-6c86-461e-952a-e5d64710b882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91418d23-9fb0-48c4-be9f-4ce7c7564df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a07f9d1-41d2-435b-a3a0-cbd4eb69daac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91418d23-9fb0-48c4-be9f-4ce7c7564df4",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "0e0dc031-13d2-48b8-8cf4-8f785a600c3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "db0c7589-dc72-441e-a1ec-94e14cdd1fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e0dc031-13d2-48b8-8cf4-8f785a600c3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75073801-940c-4faa-bf00-6c92df7fe43c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e0dc031-13d2-48b8-8cf4-8f785a600c3c",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "62dcd113-3ec3-45df-9a51-b56ecf68ff9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "85d34cc5-6d07-44f9-b2f3-91751ea2a39a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62dcd113-3ec3-45df-9a51-b56ecf68ff9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bf238ac-2983-4262-895e-c5c33db35389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62dcd113-3ec3-45df-9a51-b56ecf68ff9b",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "aa6df6e6-c855-47df-bf38-f0286886f21c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "419ca10b-ad9c-4054-b233-92f70d82df69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa6df6e6-c855-47df-bf38-f0286886f21c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f32a71-2c4d-4086-b786-960aadbd32d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6df6e6-c855-47df-bf38-f0286886f21c",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "98fdea6a-8897-4f6e-ba20-77fd82ff0641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "7c5fbd29-a920-41cd-9596-ad5fdcef2b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fdea6a-8897-4f6e-ba20-77fd82ff0641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91eac4a0-b539-4298-b538-9f0529392e77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fdea6a-8897-4f6e-ba20-77fd82ff0641",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "9eeeddd8-7712-40d3-aeea-f48fe5779557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "b8175fa0-eda7-49ce-8de7-d87533f61921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eeeddd8-7712-40d3-aeea-f48fe5779557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24875ccb-3e4b-434b-a365-967ce41c8e6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eeeddd8-7712-40d3-aeea-f48fe5779557",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "1fffb27b-ec25-4022-9243-51c89d130b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "6a42ce71-6867-48fc-b0e1-af5f7dd49b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fffb27b-ec25-4022-9243-51c89d130b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40cdbc9-9b16-438a-adcf-3273b1c23a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fffb27b-ec25-4022-9243-51c89d130b58",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "807b0e1f-6d04-4546-b5ae-54c8cb62f72e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "1b566ae6-c6bf-4a96-884b-a4002ef7e105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "807b0e1f-6d04-4546-b5ae-54c8cb62f72e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8111e973-3ceb-432c-b54c-a593b5470465",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "807b0e1f-6d04-4546-b5ae-54c8cb62f72e",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "0917e90c-f3ef-4baa-b3b6-fc7112bf324b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "7dde0861-301f-4029-a0cc-238ad192e976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0917e90c-f3ef-4baa-b3b6-fc7112bf324b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27c05b28-0d64-4f24-8190-8a43d04793c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0917e90c-f3ef-4baa-b3b6-fc7112bf324b",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "e393797b-4896-4a9e-877e-0db080b08a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "3cd6db94-b6dc-428e-8529-107aa7158f87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e393797b-4896-4a9e-877e-0db080b08a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "962797cd-be8b-412b-8b58-0d74dd889e3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e393797b-4896-4a9e-877e-0db080b08a27",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "5a269d18-b97c-4e56-a531-d5eb5a187fe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "c2c937ce-d090-41b0-bd12-2fe8c4f7c914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a269d18-b97c-4e56-a531-d5eb5a187fe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aabc71ad-199d-4b4d-ae6f-2270f93cbbc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a269d18-b97c-4e56-a531-d5eb5a187fe7",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "fc1c727c-4e85-42f9-bbd1-b1fc3dceaf0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "8ae932f8-4da3-4191-ac5e-a466d314bfd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc1c727c-4e85-42f9-bbd1-b1fc3dceaf0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e8eba8-ae92-45e2-a345-91fd19e9b0d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc1c727c-4e85-42f9-bbd1-b1fc3dceaf0e",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "2cc0ed49-c50c-4894-8cb0-ccd0cc197907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "6de1f6cd-6c67-42c5-a1ac-65a2b331412b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cc0ed49-c50c-4894-8cb0-ccd0cc197907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64e650d5-a74a-4860-b75c-ecd274eaa4a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cc0ed49-c50c-4894-8cb0-ccd0cc197907",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        },
        {
            "id": "caec0e75-4265-4e84-a283-0827a95d4c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "compositeImage": {
                "id": "f3cbad8b-fa2f-456b-81bf-ef04ccd903c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caec0e75-4265-4e84-a283-0827a95d4c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a15e4945-05b3-482b-856b-1656278377f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caec0e75-4265-4e84-a283-0827a95d4c76",
                    "LayerId": "fa27564e-fbe3-401a-9707-47441ed895aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "fa27564e-fbe3-401a-9707-47441ed895aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 52,
    "yorig": 10
}