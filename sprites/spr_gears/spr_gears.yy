{
    "id": "5b706997-3e53-4589-ac31-6ff85e8067d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gears",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 13,
    "bbox_right": 52,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e86933d-158a-42a4-a93a-55de2c46cd84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b706997-3e53-4589-ac31-6ff85e8067d9",
            "compositeImage": {
                "id": "80d99a4e-2efb-4689-a995-ae643e482a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e86933d-158a-42a4-a93a-55de2c46cd84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "977cce71-083e-45fb-9e84-623ac3c629f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e86933d-158a-42a4-a93a-55de2c46cd84",
                    "LayerId": "8bd3f4ad-8998-4492-ad8a-123b3572adb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8bd3f4ad-8998-4492-ad8a-123b3572adb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b706997-3e53-4589-ac31-6ff85e8067d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}