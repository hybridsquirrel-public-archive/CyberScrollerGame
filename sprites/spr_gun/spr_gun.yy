{
    "id": "906df299-6e6e-4d49-aa9d-6650f494ea04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a49b87f5-d08f-45a3-b033-6cdfa2b643ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "906df299-6e6e-4d49-aa9d-6650f494ea04",
            "compositeImage": {
                "id": "d6247fbc-15de-43b6-8188-3cd3a5c20c85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a49b87f5-d08f-45a3-b033-6cdfa2b643ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c812bf2-eb18-4a44-a443-4e7433615e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a49b87f5-d08f-45a3-b033-6cdfa2b643ce",
                    "LayerId": "6ebf0f25-138e-4d82-a98c-9654c6a2003d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "6ebf0f25-138e-4d82-a98c-9654c6a2003d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "906df299-6e6e-4d49-aa9d-6650f494ea04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 4
}