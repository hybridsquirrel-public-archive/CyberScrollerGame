{
    "id": "08c9a095-cdd8-432c-8028-bccdec2c8a35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guntree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 378,
    "bbox_left": 0,
    "bbox_right": 358,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7e476a4-caa7-4dfa-ba44-ddca55978138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c9a095-cdd8-432c-8028-bccdec2c8a35",
            "compositeImage": {
                "id": "07f834d1-ae30-4a13-8778-8b189c2e7577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7e476a4-caa7-4dfa-ba44-ddca55978138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd4e058-bbcc-4bff-aa96-a1ba1739d0de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7e476a4-caa7-4dfa-ba44-ddca55978138",
                    "LayerId": "5ec84887-5822-41cc-85cf-108b5c700dec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 379,
    "layers": [
        {
            "id": "5ec84887-5822-41cc-85cf-108b5c700dec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08c9a095-cdd8-432c-8028-bccdec2c8a35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 359,
    "xorig": 179,
    "yorig": 189
}