{
    "id": "66bb6308-1b24-4669-96aa-7dbace1e5e2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_courser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c60a27ce-776b-41a8-b66b-f7a9b993584f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66bb6308-1b24-4669-96aa-7dbace1e5e2d",
            "compositeImage": {
                "id": "fee11cd6-b998-446a-8817-beca7d350f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c60a27ce-776b-41a8-b66b-f7a9b993584f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a654d3d-38a6-414b-80e2-dbbbe1f7efe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c60a27ce-776b-41a8-b66b-f7a9b993584f",
                    "LayerId": "6ed35419-4103-422e-91d6-d87eb5d44bac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ed35419-4103-422e-91d6-d87eb5d44bac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66bb6308-1b24-4669-96aa-7dbace1e5e2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 15
}