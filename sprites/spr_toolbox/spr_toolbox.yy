{
    "id": "5cd6df35-5523-48d9-bbfb-c88008fc4498",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_toolbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03bc3f6a-dd65-4592-a0b6-63382dee8e83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cd6df35-5523-48d9-bbfb-c88008fc4498",
            "compositeImage": {
                "id": "b658e794-4e1e-4dfb-943d-1975c9f0efb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03bc3f6a-dd65-4592-a0b6-63382dee8e83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1463b407-133a-4693-8385-4c9358f9f819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03bc3f6a-dd65-4592-a0b6-63382dee8e83",
                    "LayerId": "8787bdd5-e0b1-4975-aeb9-906f3c5420ad"
                }
            ]
        },
        {
            "id": "630f8c8d-d20f-4fec-8f67-81cd94e6cacd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cd6df35-5523-48d9-bbfb-c88008fc4498",
            "compositeImage": {
                "id": "bab3b86d-e571-48ca-9b8e-a4fd9ab289e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "630f8c8d-d20f-4fec-8f67-81cd94e6cacd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bce85022-d2d6-4853-bda3-a8a7e101423b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "630f8c8d-d20f-4fec-8f67-81cd94e6cacd",
                    "LayerId": "8787bdd5-e0b1-4975-aeb9-906f3c5420ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "8787bdd5-e0b1-4975-aeb9-906f3c5420ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cd6df35-5523-48d9-bbfb-c88008fc4498",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 24
}