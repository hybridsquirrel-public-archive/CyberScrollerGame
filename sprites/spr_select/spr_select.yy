{
    "id": "720e6a44-c464-4b71-94f2-f57df264660a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_select",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24fd26cd-083b-45a9-a719-3d33a278729e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "720e6a44-c464-4b71-94f2-f57df264660a",
            "compositeImage": {
                "id": "b1eb7992-6f1d-4d54-96b6-625ba3d2dba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24fd26cd-083b-45a9-a719-3d33a278729e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3599da5-020a-4ab6-a2cf-1ecaae459679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24fd26cd-083b-45a9-a719-3d33a278729e",
                    "LayerId": "d5937a5f-b904-4b7d-bc59-decab85c37f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "d5937a5f-b904-4b7d-bc59-decab85c37f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "720e6a44-c464-4b71-94f2-f57df264660a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 30,
    "yorig": 30
}