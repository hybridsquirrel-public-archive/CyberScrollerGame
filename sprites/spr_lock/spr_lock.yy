{
    "id": "d5a24d00-06bb-4816-8b20-3932c75fedb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "233dd93f-79ee-4869-b00f-d742d8c10ede",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a24d00-06bb-4816-8b20-3932c75fedb4",
            "compositeImage": {
                "id": "a39defaf-d060-459a-a655-a2ca024bbbb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233dd93f-79ee-4869-b00f-d742d8c10ede",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12041d4f-0746-4908-a959-fb50beb2c58a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233dd93f-79ee-4869-b00f-d742d8c10ede",
                    "LayerId": "5e53efca-ade0-4d73-b940-c1ab4eeed31e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "5e53efca-ade0-4d73-b940-c1ab4eeed31e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5a24d00-06bb-4816-8b20-3932c75fedb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}