{
    "id": "2fa02cab-dcd1-4221-9d90-f534a12f22cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemytest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 59,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8f66560-3131-4e72-873b-1385d6f6d46c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fa02cab-dcd1-4221-9d90-f534a12f22cc",
            "compositeImage": {
                "id": "305a3758-fb31-4cec-95fc-d9e9d9f10b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f66560-3131-4e72-873b-1385d6f6d46c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "087739d7-96fe-4c5f-a762-7b1129fc5ba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f66560-3131-4e72-873b-1385d6f6d46c",
                    "LayerId": "abd92acc-5f96-4e2b-a5c1-a33b8ceb8717"
                }
            ]
        },
        {
            "id": "cec85b16-6b1b-482a-a641-ed2ba0c7130e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fa02cab-dcd1-4221-9d90-f534a12f22cc",
            "compositeImage": {
                "id": "ac239532-da5c-41ce-96e0-4c7065a68193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec85b16-6b1b-482a-a641-ed2ba0c7130e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44448890-723b-4a97-81bb-e4e793fe90e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec85b16-6b1b-482a-a641-ed2ba0c7130e",
                    "LayerId": "abd92acc-5f96-4e2b-a5c1-a33b8ceb8717"
                }
            ]
        },
        {
            "id": "b43cc6be-bafe-4828-a6ed-4e9aab9e9e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fa02cab-dcd1-4221-9d90-f534a12f22cc",
            "compositeImage": {
                "id": "32721905-143a-4f3f-9e8f-6e7533e2f7ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b43cc6be-bafe-4828-a6ed-4e9aab9e9e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24cc1b46-ac69-48b0-b6db-e0009af18f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b43cc6be-bafe-4828-a6ed-4e9aab9e9e72",
                    "LayerId": "abd92acc-5f96-4e2b-a5c1-a33b8ceb8717"
                }
            ]
        },
        {
            "id": "0cf7d9d8-a59b-4081-8273-9c35c95169b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fa02cab-dcd1-4221-9d90-f534a12f22cc",
            "compositeImage": {
                "id": "43a0bea4-1233-4b0b-bd92-e2646ece9c34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cf7d9d8-a59b-4081-8273-9c35c95169b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4749e45-6dfe-46c2-a965-cc8c1ce2fea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cf7d9d8-a59b-4081-8273-9c35c95169b9",
                    "LayerId": "abd92acc-5f96-4e2b-a5c1-a33b8ceb8717"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "abd92acc-5f96-4e2b-a5c1-a33b8ceb8717",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fa02cab-dcd1-4221-9d90-f534a12f22cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}