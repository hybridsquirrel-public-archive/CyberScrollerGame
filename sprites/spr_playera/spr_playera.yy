{
    "id": "9613e721-b592-48e1-a2a9-0c40235817e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playera",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 32,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6afa8cf-1170-4596-9bbe-01a82c6d87ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9613e721-b592-48e1-a2a9-0c40235817e6",
            "compositeImage": {
                "id": "6252a896-5cec-4ee8-b418-a21fdf454768",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6afa8cf-1170-4596-9bbe-01a82c6d87ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48dded48-be64-4ad8-9437-54ccce54faa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6afa8cf-1170-4596-9bbe-01a82c6d87ed",
                    "LayerId": "b8c5a8e3-d061-4ae5-b7f4-934f3026fd07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b8c5a8e3-d061-4ae5-b7f4-934f3026fd07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9613e721-b592-48e1-a2a9-0c40235817e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 20,
    "yorig": 27
}