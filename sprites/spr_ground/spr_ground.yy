{
    "id": "813bae74-840c-483e-bcd3-0768a471142a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f7f30fa-f21a-4408-9193-77b7ca4ae868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "813bae74-840c-483e-bcd3-0768a471142a",
            "compositeImage": {
                "id": "7f1543e4-359f-463c-a30e-68a52c4e628a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f7f30fa-f21a-4408-9193-77b7ca4ae868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07030be6-3756-4b53-8ab8-242ffcc52bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f7f30fa-f21a-4408-9193-77b7ca4ae868",
                    "LayerId": "33314a15-ccd5-449a-9fe0-62d652c4d750"
                }
            ]
        },
        {
            "id": "cecc728c-bf62-4552-b563-9c6d368e82d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "813bae74-840c-483e-bcd3-0768a471142a",
            "compositeImage": {
                "id": "8881785a-7644-4a60-a91c-07de723dc5f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cecc728c-bf62-4552-b563-9c6d368e82d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb671982-c21b-4e97-9c9a-ea92ba97ebff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cecc728c-bf62-4552-b563-9c6d368e82d8",
                    "LayerId": "33314a15-ccd5-449a-9fe0-62d652c4d750"
                }
            ]
        },
        {
            "id": "e030713c-9d8e-4521-af58-b83b937e48d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "813bae74-840c-483e-bcd3-0768a471142a",
            "compositeImage": {
                "id": "6820d594-14e3-496e-a63c-6f1be393e609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e030713c-9d8e-4521-af58-b83b937e48d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a448c2-93df-4a2a-af9d-c60628c27d36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e030713c-9d8e-4521-af58-b83b937e48d5",
                    "LayerId": "33314a15-ccd5-449a-9fe0-62d652c4d750"
                }
            ]
        },
        {
            "id": "12e09db7-caf9-4e7a-84b6-5c4d8957cc4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "813bae74-840c-483e-bcd3-0768a471142a",
            "compositeImage": {
                "id": "965ae068-0288-4fe5-9d6c-d113d1fa4d3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e09db7-caf9-4e7a-84b6-5c4d8957cc4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5f0651-b6a8-4ca7-9aeb-69dd02dc694e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e09db7-caf9-4e7a-84b6-5c4d8957cc4f",
                    "LayerId": "33314a15-ccd5-449a-9fe0-62d652c4d750"
                }
            ]
        },
        {
            "id": "c4804ca3-069a-411e-885f-674fdb3ed195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "813bae74-840c-483e-bcd3-0768a471142a",
            "compositeImage": {
                "id": "e9f982be-d7eb-45bc-becf-31358980610f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4804ca3-069a-411e-885f-674fdb3ed195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd0d999f-456d-4566-9fe4-ef6ac58ccc00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4804ca3-069a-411e-885f-674fdb3ed195",
                    "LayerId": "33314a15-ccd5-449a-9fe0-62d652c4d750"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "33314a15-ccd5-449a-9fe0-62d652c4d750",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "813bae74-840c-483e-bcd3-0768a471142a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}