{
    "id": "cc895707-b19f-4f11-8959-b058758ebbf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_congrats",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0857fc6e-d7ce-4fec-ae82-c0cb833f54ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc895707-b19f-4f11-8959-b058758ebbf1",
            "compositeImage": {
                "id": "419b2452-4762-4242-8f95-835536992af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0857fc6e-d7ce-4fec-ae82-c0cb833f54ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d75ad4ed-2bad-415b-913a-7f91c013db8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0857fc6e-d7ce-4fec-ae82-c0cb833f54ba",
                    "LayerId": "bc506c58-2105-4dbe-9023-b495369c22c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "bc506c58-2105-4dbe-9023-b495369c22c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc895707-b19f-4f11-8959-b058758ebbf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}