{
    "id": "3dcaa7ba-773f-4e9b-98c3-2367b42d6a08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_explotion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": 48,
    "bbox_right": 147,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cc39dbe-778a-40d8-9178-8574ff379692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dcaa7ba-773f-4e9b-98c3-2367b42d6a08",
            "compositeImage": {
                "id": "5c7469f2-2b8b-4c70-a8d0-e06f447f1cce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cc39dbe-778a-40d8-9178-8574ff379692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d787173b-940f-4e66-aa45-77d865a9f824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cc39dbe-778a-40d8-9178-8574ff379692",
                    "LayerId": "07616519-62d8-46b5-973e-c089d5317c8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "07616519-62d8-46b5-973e-c089d5317c8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dcaa7ba-773f-4e9b-98c3-2367b42d6a08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}