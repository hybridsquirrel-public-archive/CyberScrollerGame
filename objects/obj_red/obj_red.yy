{
    "id": "34d39518-8876-4e88-aab5-32160c4e53d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red",
    "eventList": [
        {
            "id": "1daa0167-4674-474d-a0d7-f9ef79347940",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34d39518-8876-4e88-aab5-32160c4e53d9"
        },
        {
            "id": "e317550b-a8d7-4efa-b938-7782f8261f0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "34d39518-8876-4e88-aab5-32160c4e53d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54466562-d8b0-40f2-9853-170ae271f25f",
    "visible": true
}