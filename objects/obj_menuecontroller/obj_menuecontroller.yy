{
    "id": "32102f5b-b823-4922-8979-13afb0acf33e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menuecontroller",
    "eventList": [
        {
            "id": "706d0874-6946-4c19-bd5e-eece4d41c5a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32102f5b-b823-4922-8979-13afb0acf33e"
        },
        {
            "id": "4d425d49-40a5-47bc-99d4-e8a44b090f8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32102f5b-b823-4922-8979-13afb0acf33e"
        },
        {
            "id": "c693e08c-4574-40d3-9c80-176d05cff121",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 9,
            "m_owner": "32102f5b-b823-4922-8979-13afb0acf33e"
        },
        {
            "id": "043c69ff-4bf7-4d6b-9a0c-d3f5a9e1fa99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 9,
            "m_owner": "32102f5b-b823-4922-8979-13afb0acf33e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}