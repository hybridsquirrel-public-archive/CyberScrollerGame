{
    "id": "fb658b66-1972-422e-8cbf-267d92480651",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gears",
    "eventList": [
        {
            "id": "567d74df-6a22-475d-9a22-2cdc718b7126",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb658b66-1972-422e-8cbf-267d92480651"
        },
        {
            "id": "a32f48aa-407a-409c-a73a-5990192323da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fb658b66-1972-422e-8cbf-267d92480651"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5b706997-3e53-4589-ac31-6ff85e8067d9",
    "visible": true
}