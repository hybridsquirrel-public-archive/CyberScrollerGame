{
    "id": "c108cc51-e4e7-4f98-8d9a-9f392ed25a75",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_oilbarrel",
    "eventList": [
        {
            "id": "fe025edc-81ed-46d3-8443-8150f4f993b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c108cc51-e4e7-4f98-8d9a-9f392ed25a75"
        },
        {
            "id": "c8684d95-ef0a-4f20-b97c-ed9ca275f0d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c108cc51-e4e7-4f98-8d9a-9f392ed25a75"
        },
        {
            "id": "53a8efbf-8ac9-46d8-83e5-c9035fe312ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c108cc51-e4e7-4f98-8d9a-9f392ed25a75"
        },
        {
            "id": "7bc96002-d830-4de2-b1ff-e18be8d53477",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c108cc51-e4e7-4f98-8d9a-9f392ed25a75"
        },
        {
            "id": "c5653bf4-524f-48ba-902b-12b08f1462fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c108cc51-e4e7-4f98-8d9a-9f392ed25a75"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e9c0219-2e86-4aa0-bf23-8f357f779aab",
    "visible": true
}