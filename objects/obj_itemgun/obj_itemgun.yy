{
    "id": "afa808ab-8573-487f-8b13-4f9f0b8035f0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_itemgun",
    "eventList": [
        {
            "id": "775a5436-d243-4042-b25e-1d6983182758",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "afa808ab-8573-487f-8b13-4f9f0b8035f0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "906df299-6e6e-4d49-aa9d-6650f494ea04",
    "visible": true
}