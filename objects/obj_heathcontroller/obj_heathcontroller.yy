{
    "id": "ac60d733-027f-487f-be95-72a9e86d64a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_heathcontroller",
    "eventList": [
        {
            "id": "49d802d2-c5eb-436c-88fa-17eedd994b2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac60d733-027f-487f-be95-72a9e86d64a3"
        },
        {
            "id": "9bf22004-6798-4dfa-a306-603b583990e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 9,
            "m_owner": "ac60d733-027f-487f-be95-72a9e86d64a3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}