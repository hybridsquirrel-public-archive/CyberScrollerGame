{
    "id": "c516fa3c-36d4-458e-8177-12d73a4fcd5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_startbutton",
    "eventList": [
        {
            "id": "17f6b889-5f8d-40f5-866f-3519463ff4a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c516fa3c-36d4-458e-8177-12d73a4fcd5b"
        },
        {
            "id": "de831ce8-5501-4879-a09e-15a09b1ae671",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c516fa3c-36d4-458e-8177-12d73a4fcd5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8386847a-676a-48bc-8e32-8d2d78d17dd6",
    "visible": true
}