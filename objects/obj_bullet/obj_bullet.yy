{
    "id": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "691172d4-5df0-47fb-97f3-6a11a88a2049",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f"
        },
        {
            "id": "862e8669-17ff-4637-a112-9e5e1df7c6b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f"
        },
        {
            "id": "7397f0da-8a1f-4180-9b28-e01180265ecc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "46b79e89-2732-4657-aa69-dc76af69befc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f"
        },
        {
            "id": "abf2dc3c-3102-4964-be78-20dcdb4c4005",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f"
        },
        {
            "id": "fed5d667-ed70-4878-abda-ac5e8b0a0003",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3aec1dd7-875e-447f-8616-839556b61082",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f"
        },
        {
            "id": "288f3b23-1291-4d6d-ab61-997ffd4309b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "617e8c75-596e-4f9c-806b-6c929af3eaa2",
    "visible": true
}