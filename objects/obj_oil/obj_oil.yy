{
    "id": "e142b5db-d65e-4ce0-a48a-463824f8c9c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_oil",
    "eventList": [
        {
            "id": "bf680784-a7a6-485a-9049-607fbbcb3003",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e142b5db-d65e-4ce0-a48a-463824f8c9c2"
        },
        {
            "id": "8f83076e-58f4-4df2-985a-c1e9c62b2c17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e142b5db-d65e-4ce0-a48a-463824f8c9c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb5f0e54-7bef-42b9-a3dd-a3ecd9f9c98f",
    "visible": true
}