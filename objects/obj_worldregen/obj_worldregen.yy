{
    "id": "69f991fc-7255-4d17-afed-79c43684ddb5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_worldregen",
    "eventList": [
        {
            "id": "f5cae746-f1c5-4b42-8671-e442037163bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69f991fc-7255-4d17-afed-79c43684ddb5"
        },
        {
            "id": "195a318a-00f9-4489-a268-9925da0acb8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "69f991fc-7255-4d17-afed-79c43684ddb5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1defeedb-f06e-447c-b3ca-52cd0448a843",
    "visible": true
}