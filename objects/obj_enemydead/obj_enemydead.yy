{
    "id": "9b27b5e7-aba9-46a6-a4f6-942f3d61d9db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemydead",
    "eventList": [
        {
            "id": "c929c685-a9f4-4e00-91c4-9ea01b12dd44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9b27b5e7-aba9-46a6-a4f6-942f3d61d9db"
        },
        {
            "id": "912b7721-1ffa-4929-8bad-10c2747451c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9b27b5e7-aba9-46a6-a4f6-942f3d61d9db"
        },
        {
            "id": "0bfdcd31-ee82-4d2c-97a6-d363c4efcb18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9b27b5e7-aba9-46a6-a4f6-942f3d61d9db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0a8dcc4-def9-44ec-abbb-82f434a13be6",
    "visible": true
}