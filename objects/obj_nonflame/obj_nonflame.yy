{
    "id": "5c78eb6d-252b-4b75-a579-6b25f2be0f45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_nonflame",
    "eventList": [
        {
            "id": "ccb9c0d5-365a-4050-8648-71b21fc16ded",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5c78eb6d-252b-4b75-a579-6b25f2be0f45"
        },
        {
            "id": "96e95b66-a8df-4d6f-83ba-af86fd9fb427",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5c78eb6d-252b-4b75-a579-6b25f2be0f45"
        },
        {
            "id": "b8e3e570-c402-489f-ae84-70cb3571e338",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c78eb6d-252b-4b75-a579-6b25f2be0f45"
        },
        {
            "id": "95ddbbc6-8219-44c2-8e9e-17643af05e2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "084998df-bc6f-470a-ad4b-e1d1549b6126",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5c78eb6d-252b-4b75-a579-6b25f2be0f45"
        },
        {
            "id": "c0ddb779-64fa-47e7-93ae-e37561af7096",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5c78eb6d-252b-4b75-a579-6b25f2be0f45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75334a9a-98a8-4b70-bf13-06adbd011338",
    "visible": true
}