{
    "id": "1c5499dd-e4d7-4977-b997-507db5b9f3a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lifebar",
    "eventList": [
        {
            "id": "645b2582-dcac-40b4-a9c1-1571786b9e51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c5499dd-e4d7-4977-b997-507db5b9f3a9"
        },
        {
            "id": "ae095fb1-5b03-4937-b548-6ced926198f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1c5499dd-e4d7-4977-b997-507db5b9f3a9"
        },
        {
            "id": "36a69992-d58a-4591-930e-fd65883caabd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1c5499dd-e4d7-4977-b997-507db5b9f3a9"
        },
        {
            "id": "fff5c012-31f3-40fb-8baf-84c459d25b93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "1c5499dd-e4d7-4977-b997-507db5b9f3a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3635109-652c-44d3-91a6-d97948a6efc9",
    "visible": true
}