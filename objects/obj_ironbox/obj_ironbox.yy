{
    "id": "66525e4d-6feb-45c9-9987-a669d022cbd1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ironbox",
    "eventList": [
        {
            "id": "0f79da9b-1c81-4969-b084-fdabf302a69b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "66525e4d-6feb-45c9-9987-a669d022cbd1"
        },
        {
            "id": "100a588e-6184-4aa9-8bf2-7767a4fa132b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "66525e4d-6feb-45c9-9987-a669d022cbd1"
        },
        {
            "id": "2383b1ef-bf76-4769-8c3c-51cc143c788d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "66525e4d-6feb-45c9-9987-a669d022cbd1"
        },
        {
            "id": "9a343ad9-0c11-4b97-83dc-786520c3c0a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "66525e4d-6feb-45c9-9987-a669d022cbd1"
        },
        {
            "id": "18fe5511-2f53-4a70-b6c0-c608692b78c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "66525e4d-6feb-45c9-9987-a669d022cbd1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "411c74dc-cc15-43bc-9795-f9c363dcc3c9",
    "visible": true
}