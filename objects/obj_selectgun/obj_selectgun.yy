{
    "id": "1c31c891-0554-41c8-a273-775e547e892c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_selectgun",
    "eventList": [
        {
            "id": "fa8c2f46-a168-45fe-9af6-53bd8c2d2d74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c31c891-0554-41c8-a273-775e547e892c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "720e6a44-c464-4b71-94f2-f57df264660a",
    "visible": true
}