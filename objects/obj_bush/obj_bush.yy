{
    "id": "070a6a85-b6bd-4c3c-b0ac-10b293acfd7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bush",
    "eventList": [
        {
            "id": "cfcbb485-c83c-4391-ad20-2f18ecb90809",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ffaa03b0-5e37-4b0a-b71e-35a7405085cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "070a6a85-b6bd-4c3c-b0ac-10b293acfd7c"
        },
        {
            "id": "a6cb221b-1a6c-4d46-83e1-a672280eb310",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "070a6a85-b6bd-4c3c-b0ac-10b293acfd7c"
        },
        {
            "id": "3670d03e-aaec-4ba3-b376-653f9972bf12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "070a6a85-b6bd-4c3c-b0ac-10b293acfd7c"
        },
        {
            "id": "6fcfdd27-edd6-4086-a2b2-ea7b5368d697",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "070a6a85-b6bd-4c3c-b0ac-10b293acfd7c"
        },
        {
            "id": "77546794-8b99-4a0e-b61f-c9210a056392",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "070a6a85-b6bd-4c3c-b0ac-10b293acfd7c"
        },
        {
            "id": "dd156762-5349-4425-8d28-adbdc0277764",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "070a6a85-b6bd-4c3c-b0ac-10b293acfd7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db242b74-b561-42d9-b144-c8603a8ce654",
    "visible": true
}