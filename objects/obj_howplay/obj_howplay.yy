{
    "id": "6955bd43-6931-42f7-9245-8f643202318d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_howplay",
    "eventList": [
        {
            "id": "c42daf59-c22b-4fa2-bec2-78bdef1e7bae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6955bd43-6931-42f7-9245-8f643202318d"
        },
        {
            "id": "b61fc7bb-5b06-4508-b82f-c875af3ac827",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6955bd43-6931-42f7-9245-8f643202318d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "55121ed5-c3a9-4bef-8bef-11b36b7b69f9",
    "visible": true
}