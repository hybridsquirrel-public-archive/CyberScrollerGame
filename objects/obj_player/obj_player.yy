{
    "id": "084998df-bc6f-470a-ad4b-e1d1549b6126",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "589c8a49-54b9-479c-8460-0967862474eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "084998df-bc6f-470a-ad4b-e1d1549b6126"
        },
        {
            "id": "54f2b0db-8400-4838-b36f-e2ca74e53f6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "084998df-bc6f-470a-ad4b-e1d1549b6126"
        },
        {
            "id": "07621cd3-365d-4811-9d07-c0e2ff32c6a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "084998df-bc6f-470a-ad4b-e1d1549b6126"
        },
        {
            "id": "a2b091a7-ca40-4b13-a95e-7251f3e16d71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "084998df-bc6f-470a-ad4b-e1d1549b6126"
        },
        {
            "id": "d6e2a155-e8ad-4fb3-bf3c-9272e4e4ef5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "084998df-bc6f-470a-ad4b-e1d1549b6126"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "04c498bd-7687-4fd9-a908-6fb31aa857e7",
    "visible": true
}