{
    "id": "8f6f6343-ab9a-47e7-ad97-1ec5bbf9e133",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rightlane",
    "eventList": [
        {
            "id": "0c2ff93a-56e8-47cd-a49c-0609edac7756",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f6f6343-ab9a-47e7-ad97-1ec5bbf9e133"
        },
        {
            "id": "d072e7de-d5ef-4b10-9726-3299a18dbac0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f6f6343-ab9a-47e7-ad97-1ec5bbf9e133"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "918d86e9-3796-4df0-8f00-ad7211058e01",
    "visible": true
}