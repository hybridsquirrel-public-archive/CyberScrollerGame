{
    "id": "5da74e6d-40a2-4894-80e8-95f032d0c7ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_metal",
    "eventList": [
        {
            "id": "016b35e4-5ef8-4f6c-a12f-7c6b45af661b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5da74e6d-40a2-4894-80e8-95f032d0c7ac"
        },
        {
            "id": "7deab6f1-d375-40f0-b893-cfece1d339ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5da74e6d-40a2-4894-80e8-95f032d0c7ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4f9a150-342a-48f9-a4f3-fe7f3665b085",
    "visible": true
}