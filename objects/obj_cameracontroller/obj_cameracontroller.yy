{
    "id": "20bfb9b1-00fa-4808-91c8-e68a065e22c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cameracontroller",
    "eventList": [
        {
            "id": "89cb98ba-4c02-47c2-b914-8fb0eacbf704",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20bfb9b1-00fa-4808-91c8-e68a065e22c4"
        },
        {
            "id": "6b35f29d-ef5b-4dbf-b8b4-760825b6fb31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20bfb9b1-00fa-4808-91c8-e68a065e22c4"
        },
        {
            "id": "a224c42c-07cc-42f8-8324-9dc4d131503a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 90,
            "eventtype": 9,
            "m_owner": "20bfb9b1-00fa-4808-91c8-e68a065e22c4"
        },
        {
            "id": "d7110054-80ae-4d5a-bf49-fe816bf3d0b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "20bfb9b1-00fa-4808-91c8-e68a065e22c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}