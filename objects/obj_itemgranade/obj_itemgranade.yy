{
    "id": "1c61d622-1159-477c-ba30-54ecf6406e1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_itemgranade",
    "eventList": [
        {
            "id": "2f0b4a9e-92d5-4db5-9d9e-d6563bb7eb34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c61d622-1159-477c-ba30-54ecf6406e1c"
        },
        {
            "id": "3089787b-fe0e-47c4-b678-aabfeb3b839b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1c61d622-1159-477c-ba30-54ecf6406e1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1488c194-9014-432c-bdc3-918d933790aa",
    "visible": true
}