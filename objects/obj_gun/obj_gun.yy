{
    "id": "60ea438a-c84e-407b-8391-7616c9be9293",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gun",
    "eventList": [
        {
            "id": "94e8db89-909b-4fa7-9297-9df3461d373e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60ea438a-c84e-407b-8391-7616c9be9293"
        },
        {
            "id": "a42149d7-afc2-4b79-aaae-fd8f91acabd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "60ea438a-c84e-407b-8391-7616c9be9293"
        },
        {
            "id": "262554d4-b66a-46ea-8045-9acd4ba21c1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 49,
            "eventtype": 9,
            "m_owner": "60ea438a-c84e-407b-8391-7616c9be9293"
        },
        {
            "id": "389fbd1a-686f-4cc6-b6d6-fdd788ccdd39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 9,
            "m_owner": "60ea438a-c84e-407b-8391-7616c9be9293"
        },
        {
            "id": "9be7f1eb-2373-4b69-9ef2-d604559d95bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 51,
            "eventtype": 9,
            "m_owner": "60ea438a-c84e-407b-8391-7616c9be9293"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "906df299-6e6e-4d49-aa9d-6650f494ea04",
    "visible": true
}