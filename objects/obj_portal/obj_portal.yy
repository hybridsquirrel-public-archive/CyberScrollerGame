{
    "id": "e60d77f5-7309-4ce3-8b96-1cf41ea6e69c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_portal",
    "eventList": [
        {
            "id": "4686f82e-b38b-48ce-bfd5-703d01e191de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e60d77f5-7309-4ce3-8b96-1cf41ea6e69c"
        },
        {
            "id": "aee64827-60af-4689-a8bf-10f5f6de623d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e60d77f5-7309-4ce3-8b96-1cf41ea6e69c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "50209114-b7a5-4fc5-a25a-c7339076d2be",
    "visible": true
}