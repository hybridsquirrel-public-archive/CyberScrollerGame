{
    "id": "6d51b896-115d-451a-a093-7cdb2947e30a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_itemflame",
    "eventList": [
        {
            "id": "442f3398-2c61-4130-9ffe-2c27ff7a6866",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d51b896-115d-451a-a093-7cdb2947e30a"
        },
        {
            "id": "b2014ef5-e60c-410d-94fa-a6e9854d24c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d51b896-115d-451a-a093-7cdb2947e30a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75334a9a-98a8-4b70-bf13-06adbd011338",
    "visible": true
}