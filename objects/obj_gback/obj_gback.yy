{
    "id": "37ddf50b-852a-4395-9c85-3e20e2e7393c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gback",
    "eventList": [
        {
            "id": "074e0e3b-cae8-43b7-8b80-850f4f4bf7bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37ddf50b-852a-4395-9c85-3e20e2e7393c"
        },
        {
            "id": "88b4a0c3-caff-4f3f-877f-789305518be5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "37ddf50b-852a-4395-9c85-3e20e2e7393c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "300ea9e8-5db3-4b1f-91cf-6ea71ef0f601",
    "visible": true
}