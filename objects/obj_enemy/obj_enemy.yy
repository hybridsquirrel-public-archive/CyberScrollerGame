{
    "id": "3aec1dd7-875e-447f-8616-839556b61082",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "88777ac4-667d-4036-9520-6951a3d5e7dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "85bb1396-bc1b-4cb2-8ebe-e6793bd19116",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "5bc34023-322b-4e19-861f-29852da5a52c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "ebc8f29b-6d87-49dd-9567-9db5b6170a1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "6ec4d283-142f-425a-a958-fa098208c7a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "0d509dba-9e47-4f60-9bb9-7d32452697b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f85760ab-c92f-4689-b182-7c978eaa9e33",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "6f2ba90e-4361-4d94-8d32-65057dd08cae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ffaa03b0-5e37-4b0a-b71e-35a7405085cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "d94acd66-453e-44b2-8d5b-b254ea6cb90e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "63bd2f5c-8bf4-4e2a-9a38-f7aff4eec1d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        },
        {
            "id": "ccd26402-aaa7-4ffa-adf8-ba5c8529b4a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3aec1dd7-875e-447f-8616-839556b61082"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fa02cab-dcd1-4221-9d90-f534a12f22cc",
    "visible": true
}