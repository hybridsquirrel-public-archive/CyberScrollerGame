{
    "id": "2a9a3be6-5998-4b53-8830-031dfa78739f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cluster",
    "eventList": [
        {
            "id": "b7197e47-b1f2-4db5-8120-4eda35a8bcc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a9a3be6-5998-4b53-8830-031dfa78739f"
        },
        {
            "id": "a50c7356-5237-4bd1-85b5-8de862d2e64c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a9a3be6-5998-4b53-8830-031dfa78739f"
        },
        {
            "id": "1c2cccab-37ac-4298-b73e-c8f311dc320c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2a9a3be6-5998-4b53-8830-031dfa78739f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75334a9a-98a8-4b70-bf13-06adbd011338",
    "visible": true
}