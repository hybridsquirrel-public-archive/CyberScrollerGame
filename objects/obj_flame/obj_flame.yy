{
    "id": "f85760ab-c92f-4689-b182-7c978eaa9e33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flame",
    "eventList": [
        {
            "id": "48eedcc3-a159-49ec-a08f-eeb4c2579c68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f85760ab-c92f-4689-b182-7c978eaa9e33"
        },
        {
            "id": "b3c614a8-45e1-465b-88b8-aee4807ed85d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f85760ab-c92f-4689-b182-7c978eaa9e33"
        },
        {
            "id": "b4d610b8-a7b2-4847-810d-2e1dc863e730",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "46b79e89-2732-4657-aa69-dc76af69befc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f85760ab-c92f-4689-b182-7c978eaa9e33"
        },
        {
            "id": "025cb9eb-5e08-47b0-a2c5-142b40a5ff47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "f85760ab-c92f-4689-b182-7c978eaa9e33"
        },
        {
            "id": "a5126c48-2d2f-4ad9-8925-626193496788",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3aec1dd7-875e-447f-8616-839556b61082",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f85760ab-c92f-4689-b182-7c978eaa9e33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75334a9a-98a8-4b70-bf13-06adbd011338",
    "visible": true
}