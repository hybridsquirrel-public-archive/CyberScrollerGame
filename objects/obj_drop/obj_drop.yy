{
    "id": "402c2937-2462-4385-8eb6-fafcb8f9c25e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drop",
    "eventList": [
        {
            "id": "7a7297cb-26e4-4d79-af2f-26192241d28c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "402c2937-2462-4385-8eb6-fafcb8f9c25e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "84aaa0cc-59ba-4844-8042-a8643902a7ee",
    "visible": true
}