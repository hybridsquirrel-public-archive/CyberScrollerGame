{
    "id": "46b79e89-2732-4657-aa69-dc76af69befc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ground",
    "eventList": [
        {
            "id": "b4d47e28-b097-444d-b9d6-3f2a4914dcd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46b79e89-2732-4657-aa69-dc76af69befc"
        },
        {
            "id": "e7a6462d-378a-42f1-aee3-dcbd9d9e356b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "46b79e89-2732-4657-aa69-dc76af69befc"
        },
        {
            "id": "c6088841-b49b-46ed-9099-788715a758f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46b79e89-2732-4657-aa69-dc76af69befc"
        },
        {
            "id": "782101ff-6c18-490b-b775-286178742bc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ffaa03b0-5e37-4b0a-b71e-35a7405085cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "46b79e89-2732-4657-aa69-dc76af69befc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "813bae74-840c-483e-bcd3-0768a471142a",
    "visible": true
}