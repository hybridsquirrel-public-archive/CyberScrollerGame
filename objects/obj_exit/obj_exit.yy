{
    "id": "fe141f21-ee1d-44b3-99ca-602b266ad614",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exit",
    "eventList": [
        {
            "id": "619351f2-90e6-47b3-b437-4fc34a3f0f0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe141f21-ee1d-44b3-99ca-602b266ad614"
        },
        {
            "id": "450ddd2b-6e34-4a87-adae-968212e80837",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fe141f21-ee1d-44b3-99ca-602b266ad614"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "61270b0f-458b-4e5e-92f0-1e0946ded120",
    "visible": true
}