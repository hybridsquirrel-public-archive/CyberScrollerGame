{
    "id": "ffaa03b0-5e37-4b0a-b71e-35a7405085cf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_explosive",
    "eventList": [
        {
            "id": "ede2d6a4-8bc1-46b3-9bd5-5ceb69c09d3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ffaa03b0-5e37-4b0a-b71e-35a7405085cf"
        },
        {
            "id": "eef748ba-8992-4871-9a8e-8618ec7b7817",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ffaa03b0-5e37-4b0a-b71e-35a7405085cf"
        },
        {
            "id": "a1294efe-c397-46ca-a52e-bf1ac0c8dd95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "084998df-bc6f-470a-ad4b-e1d1549b6126",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ffaa03b0-5e37-4b0a-b71e-35a7405085cf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3dcaa7ba-773f-4e9b-98c3-2367b42d6a08",
    "visible": true
}