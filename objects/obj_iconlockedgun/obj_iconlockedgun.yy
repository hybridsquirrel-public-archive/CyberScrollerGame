{
    "id": "0f6f1dc8-5c60-43f9-9a4b-52d1a995e396",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_iconlockedgun",
    "eventList": [
        {
            "id": "bd0e6d10-37bf-419b-b0ff-5b47f1ae772e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0f6f1dc8-5c60-43f9-9a4b-52d1a995e396"
        },
        {
            "id": "c59d264f-55e8-42a9-ade1-462c7b24ab02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f6f1dc8-5c60-43f9-9a4b-52d1a995e396"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d5a24d00-06bb-4816-8b20-3932c75fedb4",
    "visible": true
}