{
    "id": "d99d58e5-13cc-4d5f-8f57-5c403e1af655",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_healthp",
    "eventList": [
        {
            "id": "c754d0ff-eb49-4452-8a51-e3392a4e70db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d99d58e5-13cc-4d5f-8f57-5c403e1af655"
        },
        {
            "id": "7dd0102b-98f1-4662-aad7-210ff646f4c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d99d58e5-13cc-4d5f-8f57-5c403e1af655"
        },
        {
            "id": "20ed4acc-f3bb-4893-b57e-d04c7d647cc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d99d58e5-13cc-4d5f-8f57-5c403e1af655"
        },
        {
            "id": "5b1ccf46-5ac6-4074-892a-730ff8f3ef0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d99d58e5-13cc-4d5f-8f57-5c403e1af655"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f2fdd6e-982e-4430-b1bc-215c3baa47c3",
    "visible": true
}