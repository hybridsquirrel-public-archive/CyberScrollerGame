{
    "id": "bdc13a28-01c6-4bf5-82d3-334d9fec1fff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_leftlane",
    "eventList": [
        {
            "id": "1721e5ab-b6d6-406d-8348-f19d5318533e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdc13a28-01c6-4bf5-82d3-334d9fec1fff"
        },
        {
            "id": "c82b6526-6574-4fd1-9327-db1cc69fc796",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bdc13a28-01c6-4bf5-82d3-334d9fec1fff"
        },
        {
            "id": "a52ec3c8-139f-4c68-b84a-2104a39f691b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bdc13a28-01c6-4bf5-82d3-334d9fec1fff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "918d86e9-3796-4df0-8f00-ad7211058e01",
    "visible": true
}