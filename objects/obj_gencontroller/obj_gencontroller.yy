{
    "id": "14c93929-165e-4de8-bdf3-33c144009cff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gencontroller",
    "eventList": [
        {
            "id": "13a8f2f7-a152-4e84-8ca6-bbe9293e11af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14c93929-165e-4de8-bdf3-33c144009cff"
        },
        {
            "id": "08e39939-850f-494e-9814-7d498f63b7ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "14c93929-165e-4de8-bdf3-33c144009cff"
        },
        {
            "id": "baf375c8-4978-4860-a96d-e9207bb66f19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 9,
            "m_owner": "14c93929-165e-4de8-bdf3-33c144009cff"
        },
        {
            "id": "bb4c7871-6db7-4250-ad6f-c95f7f2a63c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "14c93929-165e-4de8-bdf3-33c144009cff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}