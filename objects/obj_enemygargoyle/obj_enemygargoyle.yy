{
    "id": "d8469848-2ecf-41f5-b381-14ae229c29cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemygargoyle",
    "eventList": [
        {
            "id": "9f1128a6-0b87-49a8-a169-4f348e52c4c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "045738c4-ab83-4cf8-876a-9a429a3be1c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "de0f4253-d537-4e3b-9b32-f4ffbced1205",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "7449fe90-2c6e-4bfb-87d9-40574a33ea1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "7b81da30-1422-4f45-af65-a037506ab2a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f85760ab-c92f-4689-b182-7c978eaa9e33",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "60c5b855-7d93-49ef-9671-bd2e55d55c85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "19eec0a7-ec8b-438f-9f5e-e599e813fb53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ffaa03b0-5e37-4b0a-b71e-35a7405085cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "1c61a6ec-a4e4-4850-b696-7ce8b87e77aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "637d1933-b1b4-4ce6-b9c2-4c0104b5293f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "83e7dca9-3c48-490a-a34d-4e67ed6c6ab1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "8f671ccb-eb81-4fa4-a4f1-70e0d36352fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "e6ddf648-cc78-444e-8bc2-960e819d4c4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "a3018ac4-c00e-42d7-b2c1-5dcca51dbc10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "775c4e25-744d-4564-948e-29da973c6b95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "8cb22d0a-5d38-4497-af93-8594ec9e0833",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        },
        {
            "id": "ffdff947-2ff6-497e-b40c-d8589408b51d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "d8469848-2ecf-41f5-b381-14ae229c29cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cf3485e5-5029-49b4-84e2-d703569b3b97",
    "visible": true
}