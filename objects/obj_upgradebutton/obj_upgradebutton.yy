{
    "id": "39fea58d-af0d-43be-bf45-d01729987a17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_upgradebutton",
    "eventList": [
        {
            "id": "4dbb187a-c662-450b-b9be-f55cc1c03473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "39fea58d-af0d-43be-bf45-d01729987a17"
        },
        {
            "id": "1ba82f4d-c643-4c54-b703-6f0b18162e9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "39fea58d-af0d-43be-bf45-d01729987a17"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8a304a0-09c1-4ea1-94c4-48719cd0cfbe",
    "visible": true
}