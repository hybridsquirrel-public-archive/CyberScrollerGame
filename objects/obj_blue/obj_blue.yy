{
    "id": "a98878b9-bb3f-4502-8ce6-e3e07d237c70",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blue",
    "eventList": [
        {
            "id": "b3b8258e-96f3-4e2c-8991-13d6f3935d4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a98878b9-bb3f-4502-8ce6-e3e07d237c70"
        },
        {
            "id": "988598ba-8c1c-4008-810d-0a4956574942",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a98878b9-bb3f-4502-8ce6-e3e07d237c70"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "592af05c-1095-43ea-a868-fb589bad27dd",
    "visible": true
}