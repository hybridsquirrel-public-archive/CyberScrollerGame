{
    "id": "4a64278d-abeb-4acb-829c-10a1c023232d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sback",
    "eventList": [
        {
            "id": "d4f04562-38ae-4d73-b853-6f8da9789ce0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4a64278d-abeb-4acb-829c-10a1c023232d"
        },
        {
            "id": "a0c91a24-35c5-472b-92de-5d3ae529884e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a64278d-abeb-4acb-829c-10a1c023232d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "300ea9e8-5db3-4b1f-91cf-6ea71ef0f601",
    "visible": true
}