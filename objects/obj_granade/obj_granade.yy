{
    "id": "75143f03-0e85-4b92-acc1-38010538a008",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_granade",
    "eventList": [
        {
            "id": "3bdfb252-0671-41b3-80ac-e8aac5545ada",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75143f03-0e85-4b92-acc1-38010538a008"
        },
        {
            "id": "f86efbe5-9948-4a2a-b529-6fb21770aa1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "75143f03-0e85-4b92-acc1-38010538a008"
        },
        {
            "id": "ea97a33e-7f2d-4433-a1fe-4bf994fe89e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "46b79e89-2732-4657-aa69-dc76af69befc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "75143f03-0e85-4b92-acc1-38010538a008"
        },
        {
            "id": "41c47673-23e5-43b8-884b-e0e4f5430677",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75143f03-0e85-4b92-acc1-38010538a008"
        },
        {
            "id": "26c019a2-84be-462e-a1cf-278331e8cafb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "75143f03-0e85-4b92-acc1-38010538a008"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1488c194-9014-432c-bdc3-918d933790aa",
    "visible": true
}