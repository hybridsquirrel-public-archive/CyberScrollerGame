{
    "id": "17da4bba-02c5-494b-938a-c4437a34a456",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_toolbox",
    "eventList": [
        {
            "id": "2bc52cc9-6869-4d21-a501-a224d88650be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "17da4bba-02c5-494b-938a-c4437a34a456"
        },
        {
            "id": "381cbe6b-833b-4679-a20e-0622ef9a9761",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "17da4bba-02c5-494b-938a-c4437a34a456"
        },
        {
            "id": "7b16fc86-3caf-46f4-add9-eef880b3d9b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "17da4bba-02c5-494b-938a-c4437a34a456"
        },
        {
            "id": "9d158ffb-e2ad-4a23-9c26-5bee096bba01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "17da4bba-02c5-494b-938a-c4437a34a456"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5cd6df35-5523-48d9-bbfb-c88008fc4498",
    "visible": true
}